#include "CppUnitTest.h"

#include "continuations.h"

#include <future>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace futures_with_continuations
{
	TEST_CLASS(ContinuationsTest)
	{
	public:

		TEST_METHOD(Execute_ForContinuationConstructedWithSingleCapturedVariableLambda_UpdatesCapturedVariable)
		{
			// Arrange
			auto testValue = 4.0;
			auto result = 0.0;
			auto squareRootFn = [&result, testValue](){ result = sqrt(testValue); };
			auto expected = sqrt(testValue);
			std::future<void> unusedFuture;
			auto continuation = make_unique_continuation(squareRootFn, unusedFuture.share());

			// Act
			continuation->execute();

			// Assert
			Assert::AreEqual(expected, result, L"Continuation when executed didn't produce the expected square root value");
		}

		TEST_METHOD(Execute_ForContinuationConstructedWithMultipleCapturedVariableLambda_UseCapturedVariablesToProduceExpectedResult)
		{
			// Arrange
			const auto testValue1 = 4.0;
			const std::size_t testValue2 = 7;
			const auto testValue3 = 5.0f;
			auto result = 0.0;
			auto equationFn = [&result, testValue1, testValue2, testValue3](){ result = (testValue1 + static_cast<double>(testValue2)) - static_cast<double>(testValue3); };
			const auto expected = (testValue1 + static_cast<double>(testValue2)) - static_cast<double>(testValue3);
			std::future<void> unusedFuture;
			auto continuation = make_unique_continuation(equationFn, unusedFuture.share());

			// Act
			continuation->execute();

			// Assert
			Assert::AreEqual(expected, result, L"Continuation when executed didn't produce the expected result from the equation");
		}

		TEST_METHOD(Execute_ForContinuationConstructedWithSingleArgLambda_ExecutesTheLambda)
		{
			// Arrange
			auto result = 0;
			auto expected = 4;
			auto equationFn = [&result](const int future){ result = future; };
			std::future<int> future = std::async(std::launch::deferred, [expected](){ return expected; });;
			auto continuation = make_unique_continuation(equationFn, future.share());

			// Act
			continuation->execute();

			// Assert
			Assert::AreEqual(expected, result, L"Continuation when executed didn't produce the expected result");
		}

		TEST_METHOD(Continuation_WhenPassedAFunctionObjectAndParameter_CopiesTheFunctionObjectAndParameter)
		{
			// Arrange
			auto testValue = 4.0;
			auto result = 0.0;
			auto expected = sqrt(testValue);
			std::future<double> future = std::async(std::launch::deferred, [testValue](){ return testValue; });
			std::unique_ptr<IContinuation> continuation;

			// Act
			{
				std::function<void(const double)> squareRootFn = [&result](const double futureResult){ result = sqrt(futureResult); };
				continuation = make_unique_continuation(squareRootFn, future.share());

				// squareRootFn and testValue should go out of scope now and both should be in an invalid state,
				// meaning we will have to have copied both to be able to call execute
			}
			continuation->execute();

			// Assert
			Assert::AreEqual(expected, result, L"Continuation when executed didn't produce the expected square root value after the source function object has gone out of scope");
		}

	};
}