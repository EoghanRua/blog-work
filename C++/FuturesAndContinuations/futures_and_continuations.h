#pragma once

#include "continuations.h"

#include <future>
#include <vector>

namespace futures_with_continuations
{
	template<typename T>
	class FutureWithContinuations final
	{
	public:
		template<typename Function, typename... ArgTypes>
		FutureWithContinuations(Function&& futureFunction, ArgTypes&&... args)
			: m_continuations()
			, m_future()
		{
			// Let the concurrency runtime decide whether to run asynchronously or to defer running of the future
			auto future = std::async(std::forward<Function>(futureFunction), std::forward<ArgTypes>(args)...);
			m_future = future.share();
		}

		~FutureWithContinuations(){}

		// Disable copying and moving of a FutureWithContinuations
		FutureWithContinuations(const FutureWithContinuations&) = delete;
		FutureWithContinuations(FutureWithContinuations&&) = delete;
		FutureWithContinuations& operator=(const FutureWithContinuations&) = delete;
		FutureWithContinuations& operator=(FutureWithContinuations&&) = delete;

		template<typename Function>
		void then(const Function& continuationFunction)
		{
			auto continuation = make_unique_continuation(continuationFunction, m_future);
			m_continuations.push_back(std::move(continuation));
		}

		void wait()
		{
			if (!m_future.valid())
				throw std::future_error(std::future_errc::no_state);

			m_future.wait();
			for (const auto& continuation : m_continuations)
			{
				continuation->execute();
			}
			m_continuations.clear();
		}

		T get()
		{
			return m_future.get();
		}

	private:
		std::vector<std::unique_ptr<IContinuation>> m_continuations;
		std::shared_future<T> m_future;
	};
}