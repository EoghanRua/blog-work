#include "CppUnitTest.h"
#include <Thread.h>

using namespace ConcRT;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ConcRT_Test
{
	class ThreadTestImpl : public Thread
	{
	public:
		ThreadTestImpl() : Thread(){}
		~ThreadTestImpl(){}

	protected:
		void run() override{/* DO NOTHING */}
	};

	TEST_CLASS(ThreadTest)
	{
	public:
		
		TEST_METHOD(Start_CalledMultipleTimes_ReturnsFalseAfterInitialCall)
		{
			// Arrange:
			ThreadTestImpl testThread;

			// Act:
			testThread.start();
			const auto result = testThread.start();
			testThread.join();

			// Assert:
			Assert::IsFalse(result, L"Second call to start should have returned false");
		}

		TEST_METHOD(Start_CalledMultipleTimesInterposedWithJoinCall_ReturnsTrue)
		{
			// Arrange:
			ThreadTestImpl testThread;

			// Act:
			testThread.start();
			testThread.join();
			const auto result = testThread.start();
			testThread.join();

			// Assert:
			Assert::IsTrue(result, L"Second call to start should have returned true, since we called join before calling start again");
		}

		TEST_METHOD(Join_CalledBeforeStart_ReturnsFalse)
		{
			// Arrange:
			ThreadTestImpl testThread;

			// Act:
			const auto result = testThread.join();

			// Assert:
			Assert::IsFalse(result, L"Call to join should have returned false as thread hasn't been started");
		}

		TEST_METHOD(Join_CalledMultipleTimesAfterStart_ReturnsFalse)
		{
			// Arrange:
			ThreadTestImpl testThread;

			// Act:
			testThread.start();
			testThread.join();
			const auto result = testThread.join();

			// Assert:
			Assert::IsFalse(result, L"Call to join should have returned false as join has already been called on the thread");
		}

	};
}