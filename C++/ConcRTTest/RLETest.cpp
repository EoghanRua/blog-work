#include "CppUnitTest.h"
#include <deque>
#include <iterator>
#include <RLE.h>
#include <string>

using namespace ConcRT;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace ConcRT_Test
{
	TEST_CLASS(ConcRT_RLETest)
	{
	public:

		TEST_METHOD(encode_StringContainingMultipleRuns_ReturnsEncodedMiddleSubstringForIteratorsToMiddleSubstring)
		{
			// Arrange:
			string input = "222333444";
			stringstream expectedSS; 
			expectedSS << (char)3 << "3";

			auto start = input.cbegin();
			advance(start, 3);
			auto end = input.cbegin();
			advance(end, 6);

			// Act:
			auto result = RLE::encode<string>(start, end);

			// Assert:
			Assert::AreEqual(expectedSS.str(), result);
		}

		TEST_METHOD(encode_StringContainingOnlyRuns_ReturnsEncodedRuns)
		{
			// Arrange:
			string input = "2222255556666688833";
			stringstream expectedSS;
			expectedSS << (char)5 << "2" << (char)4 << "5" << (char)5 << "6" << (char)3 << "8" << (char)2 << "3";

			// Act:
			auto result = RLE::encode<string>(input.cbegin(), input.cend());

			// Assert:
			Assert::AreEqual(expectedSS.str(), result);
		}

		TEST_METHOD(encode_StringContainingBothRunsAndSingleChars_ReturnsEncodedRunsAndSingleChars)
		{
			// Arrange:
			string input = "2222251855566476668883";
			stringstream expectedSS;
			expectedSS << (char)5 << "2" << (char)-3 << "518" << (char)3 << "5" << (char)2 << "6" << (char)-2 << "47" << (char)3 << "6" << (char)3 << "8" << (char)-1 << "3";

			// Act:
			auto result = RLE::encode<string>(input.cbegin(), input.cend());

			// Assert:
			Assert::AreEqual(expectedSS.str(), result);
		}

		TEST_METHOD(encode_StringContainingDoubleDigitRuns_ReturnsEncodedDoubleDigitRuns)
		{
			// Arrange:
			string input = "22222222222222";
			stringstream expectedSS;
			expectedSS << (char)14 << '2';

			// Act:
			auto result = RLE::encode<string>(input.cbegin(), input.cend());

			// Assert:
			Assert::AreEqual(expectedSS.str(), result);
		}

		TEST_METHOD(encode_StringContainingDoubleDigitRunsAndSingleChars_ReturnsEncodedDoubleDigitRunsAndSingleChars)
		{
			// Arrange:
			string input = "07222222222222224379111";
			stringstream expectedSS;
			expectedSS << (char)-2 << "07" << (char)14 << "2" << (char)-4 << "4379" << (char)3 << "1";

			// Act:
			auto result = RLE::encode<string>(input.cbegin(), input.cend());

			// Assert:
			Assert::AreEqual(expectedSS.str(), result);
		}

		TEST_METHOD(encode_StringContainingOnlySingleChars_ReturnsEncodedSingleChars)
		{
			// Arrange:
			string input = "123456789123456789123456789";
			stringstream expectedSS;
			expectedSS << (char)-27 << "123456789123456789123456789";

			// Act:
			auto result = RLE::encode<string>(input.cbegin(), input.cend());

			// Assert:
			Assert::AreEqual(expectedSS.str(), result);
		}

		TEST_METHOD(encode_DequeContainingOnlySingleChars_ReturnsEncodedSingleChars)
		{
			// Arrange:
			string inputString = "123456789123456789123456789";
			deque<char> input(inputString.cbegin(), inputString.cend());
			stringstream expectedSS;
			expectedSS << (char)-27 << "123456789123456789123456789";
			string expectedS = expectedSS.str();
			deque<char> expected(expectedS.cbegin(), expectedS.cend());

			// Act:
			auto result = RLE::encode<deque<char>>(input.cbegin(), input.cend());

			// Assert:
			Assert::IsTrue(expected == result);
		}

		TEST_METHOD(decode_StringContainingOnlyRuns_ReturnsDecodedRUns)
		{
			// Arrange:
			stringstream inputSS;
			inputSS << (char)5 << "2" << (char)4 << "5" << (char)5 << "6" << (char)3 << "8" << (char)2 << "3";
			string input = inputSS.str();
			string expected = "2222255556666688833";

			// Act:
			auto result = RLE::decode<string>(input.cbegin(), input.cend());

			// Assert:
			Assert::AreEqual(expected, result);
		}

		TEST_METHOD(decode_StringContainingBothRunsAndSingleChars_ReturnsDecodedRunsAndSingleChars)
		{
			// Arrange:
			stringstream inputSS;
			inputSS << (char)5 << "2" << (char)-3 << "518" << (char)3 << "5" << (char)2 << "6" << (char)-2 << "47" << (char)3 << "6" << (char)3 << "8" << (char)-1 << "3";
			string input = inputSS.str();
			string expected = "2222251855566476668883";

			// Act:
			auto result = RLE::decode<string>(input.cbegin(), input.cend());

			// Assert:
			Assert::AreEqual(expected, result);
		}

		TEST_METHOD(decode_StringContainingDoubleDigitRunsAndSingleChars_ReturnsDecodedRunsAndSingleChars)
		{
			// Arrange:
			stringstream inputSS;
			inputSS << (char)-27  << "123456789123456789123456789" << char(17) << "3";
			string input = inputSS.str();
			string expected = "12345678912345678912345678933333333333333333";

			// Act:
			auto result = RLE::decode<string>(input.cbegin(), input.cend());

			// Assert:
			Assert::AreEqual(expected, result);
		}

		TEST_METHOD(decode_DequeContainingRunsAndSingleChars_ReturnsDecodedRunsAndSingleChars)
		{
			// Arrange:
			stringstream inputSS;
			inputSS << (char)5 << "2" << (char)-3 << "518" << (char)3 << "5" << (char)2 << "6" << (char)-2 << "47" << (char)3 << "6" << (char)3 << "8" << (char)-1 << "3";
			string inputS = inputSS.str();
			deque<char> input(inputS.cbegin(), inputS.cend());
			string expectedString = "2222251855566476668883";
			deque<char> expected(expectedString.cbegin(), expectedString.cend());

			// Act:
			auto result = RLE::decode<deque<char>>(input.cbegin(), input.cend());

			// Assert:
			Assert::IsTrue(expected == result);
		}

		TEST_METHOD(decode_IteratorsReferencingMiddleSubString_ReturnsDecodedSubString)
		{
			// Arrange:
			stringstream inputSS;
			inputSS << (char)3 << "2" << (char)3 << "3" << (char)3 << "4";
			string input = inputSS.str();
			string expected = "333";

			auto start = input.cbegin();
			advance(start, 2);
			auto end = input.cbegin();
			advance(end, 4);

			// Act:
			auto result = RLE::decode<string>(start, end);

			// Assert:
			Assert::AreEqual(expected, result);
		}
	};
}