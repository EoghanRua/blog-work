#pragma once

#include <string>

namespace BinaryFloatingPoint
{
	bool convert_base(std::string& number, const std::size_t currentBase, const std::size_t wantedBase);
	std::string convert_base(std::size_t value, const std::size_t currentBase, const std::size_t wantedBase);

	float add_ieee(const float left, const float right);
	float subtract_ieee(const float left, const float right);
	float multiply_ieee(const float left, const float right);
	bool divide_ieee(const float numerator, const float denominator, float& result);
}