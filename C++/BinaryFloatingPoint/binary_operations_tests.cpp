#include "binary_operations.h"
#include "CppUnitTest.h"

#include <limits>
#include <vector>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace
{
	const float MaxErrorTolerance = 0.0001f;
	bool AreAlmostEqual(const float expected, const float actual, const float tolerance = numeric_limits<float>::epsilon())
	{
		return (fabs(expected - actual) < tolerance);
	}
}

namespace BinaryFloatingPoint
{
	TEST_CLASS(BinaryOperationsTest)
	{
	public:
		
		TEST_METHOD(convertBase_ForEmptyStringNumber_ReturnsFalse)
		{
			// Arrange:
			string emptyString("");

			// Act:
			const auto result = convert_base(emptyString, 2, 2);

			// Assert:
			Assert::IsFalse(result, L"convert_base should return false being passed an empty string");
		}

		TEST_METHOD(convertBase_WhenPassedSameBaseTwice_ReturnsFalse)
		{
			// Arrange:
			string expected("123");

			// Act:
			const auto result = convert_base(expected, 2, 2);

			// Assert:
			Assert::IsFalse(result, L"convert_base should return false for same base passed twice");
		}

		TEST_METHOD(convertBase_WhenPassedCurrentBaseOutsideOfExpectedRange_ReturnsFalse)
		{
			// Arrange:
			string expected("123");

			// Act:
			const auto result = convert_base(expected, 1, 2);

			// Assert:
			Assert::IsFalse(result, L"convert_base should return false for invalid currentBase");
		}

		TEST_METHOD(convertBase_WhenPassedWantedBaseOutsideOfExpectedRange_ReturnsFalse)
		{
			// Arrange:
			string expected("123");

			// Act:
			const auto result = convert_base(expected, 2, 1);

			// Assert:
			Assert::IsFalse(result, L"convert_base should return false for invalid wantedBase");
		}

		TEST_METHOD(convertBase_WhenPassedNumberInBase10_ReturnsNumberConvertedToWantedBase2)
		{
			// Arrange:
			string source("123");
			string expected("1111011");	// 123 converted to base 2

			// Act:
			convert_base(source, 10, 2);

			// Assert:
			Assert::AreEqual(expected, source, L"Number converted by convert_base should be same as expected");
		}

		TEST_METHOD(convertBase_WhenPassedHexadecimalNumberInBase_ReturnsNumberConvertedToWantedBase10)
		{
			// Arrange:
			string source("F1A");
			string expected("3866");	// F1A converted to base 10

			// Act:
			const auto result = convert_base(source, 16, 10);

			// Assert:
			Assert::AreEqual(expected, source, L"Hex number converted by convert_base should be same as expected");
		}

		TEST_METHOD(addIEEE_WhenPassedSingleDecimalFloats_ReturnsTheCorrectSum)
		{
			// Arrange:
			const auto value = 10.5f;
			const auto expected = value + value;

			// Act:
			const auto result = add_ieee(value, value);

			// Assert:
			Assert::AreEqual(expected, result, L"add_ieee should have produced the sum of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(addIEEE_WhenPassedDoubleDecimalFloats_ReturnsTheCorrectSum)
		{
			// Arrange:
			const auto value = 10.25f;
			const auto expected = value + value;

			// Act:
			const auto result = add_ieee(value, value);

			// Assert:
			Assert::AreEqual(expected, result, L"add_ieee should have produced the sum of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(addIEEE_WhenPassedDifferentLengthDecimalFloats_ReturnsTheCorrectSum)
		{
			// Arrange:
			const auto left = 10.625f;
			const auto right = 2.25f;
			const auto expected = left + right;

			// Act:
			const auto result = add_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"add_ieee should have produced the sum of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(addIEEE_WhenPassedFractionalOnlyFloats_ReturnsTheCorrectSum)
		{
			// Arrange:
			const auto left = 0.25f;
			const auto right = 0.85f;
			const auto expected = left + right;

			// Act:
			const auto result = add_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"add_ieee should have produced the sum of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(addIEEE_WhenPassedIntegerOnlyFloats_ReturnsTheCorrectSum)
		{
			// Arrange:
			const auto left = 2.0f;
			const auto right = 8.0f;
			const auto expected = left + right;

			// Act:
			const auto result = add_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"add_ieee should have produced the sum of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(addIEEE_WhenPassedNegativeAndPositiveFloats_ReturnsTheCorrectSum)
		{
			// Arrange:
			const auto left = 20.25f;
			const auto right = -45.5f;
			const auto expected = left + right;

			// Act:
			const auto result = add_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"add_ieee should have produced the sum of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(addIEEE_WhenPassedNegativeFloats_ReturnsTheCorrectSum)
		{
			// Arrange:
			const auto left = -2.79f;
			const auto right = -13.4f;
			const auto expected = left + right;

			// Act:
			const auto result = add_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"add_ieee should have produced the sum of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(addIEEE_WhenPassedZeroAndFloat_ReturnsTheFloat)
		{
			// Arrange:
			const auto left = -1.8f;
			const auto right = 0.0f;
			const auto expected = left;

			// Act:
			const auto result = add_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"add_ieee should have produced the sum of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(addIEEE_ZeroTwice_ReturnsZero)
		{
			// Arrange:
			const auto zero = 0.0f;

			// Act:
			const auto result = add_ieee(zero, zero);

			// Assert:
			Assert::AreEqual(zero, result, L"add_ieee should have produced the sum of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(addIEEE_BlogExample_ReturnsTheCorrectResult)
		{
			// Arrange:
			const auto left = 14.625f;
			const auto right = 2.5f;
			const auto expected = left + right;

			// Act:
			const auto result = add_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"add_ieee should have produced the sum of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(subtractIEEE_IntegerOnlyFloats_ReturnsTheCorrectResult)
		{
			// Arrange:
			const auto left = 10.0f;
			const auto right = 5.0f;
			const auto expected = left - right;

			// Act:
			const auto result = subtract_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"subtract_ieee should have produced the subtraction of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(subtractIEEE_SubtractingLargerIntFromSmallerInt_ReturnsANegativeNumber)
		{
			// Arrange:
			const auto left = 8.0f;
			const auto right = 9.0f;
			const auto expected = left - right;

			// Act:
			const auto result = subtract_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"subtract_ieee should have produced the subtraction of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(subtractIEEE_SubtractingIntTwiceValueOfSmallerInt_ReturnsNegativeSmallerInt)
		{
			// Arrange:
			const auto left = 5.0f;
			const auto right = 10.0f;
			const auto expected = left - right;

			// Act:
			const auto result = subtract_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"subtract_ieee should have produced the subtraction of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(subtractIEEE_SubtractingSameIntFromItself_ReturnsZero)
		{
			// Arrange:
			const auto value = 8.0f;
			const auto expected = 0.0f;

			// Act:
			const auto result = subtract_ieee(value, value);

			// Assert:
			Assert::AreEqual(expected, result, L"subtract_ieee should have produced the subtraction of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(subtractIEEE_FractionalOnlyFloats_ReturnsTheCorrectResult)
		{
			// Arrange:
			const auto left = 0.625f;
			const auto right = 0.5f;
			const auto expected = left - right;

			// Act:
			const auto result = subtract_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"subtract_ieee should have produced the subtraction of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(subtractIEEE_DifferentLengthDecimalFloats_ReturnsTheCorrectResult)
		{
			// Arrange:
			const auto left = 0.625f;
			const auto right = 3.15f;
			const auto expected = left - right;

			// Act:
			const auto result = subtract_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"subtract_ieee should have produced the subtraction of both floating point numbers according to the IEEE standard");
		}

		TEST_METHOD(subtractIEEE_SubtractingFloatFromZero_ReturnsFloatNegated)
		{
			// Arrange:
			const auto left = 0.0f;
			const auto right = 3.15f;
			const auto expected = right * -1.0f;

			// Act:
			const auto result = subtract_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"subtract_ieee should have produced the value of the float negated, after attempting to subtract a float from zero");
		}

		TEST_METHOD(subtractIEEE_SubtractingZeroFromFloat_ReturnsFloat)
		{
			// Arrange:
			const auto left = 7.06f;
			const auto right = 0.0f;
			const auto expected = left;

			// Act:
			const auto result = subtract_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"subtract_ieee should have returned the float, after attempting to subtract zero from it");
		}

		TEST_METHOD(subtractIEEE_BlogExample_ReturnsCorrectResult)
		{
			// Arrange:
			const auto left = 2.5f;
			const auto right = 14.625f;
			const auto expected = left - right;

			// Act:
			const auto result = subtract_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"subtract_ieee should have returned the float, after attempting to subtract zero from it");
		}

		TEST_METHOD(multiplyIEEE_FloatByZero_ReturnsZero)
		{
			// Arrange:
			const auto value = 2.0f;
			const auto zero = 0.0f;

			// Act:
			const auto result = multiply_ieee(value, zero);

			// Assert:
			Assert::AreEqual(zero, result, L"multiply_ieee should have returned zero, after attempting to multiply a value by zero");
		}

		TEST_METHOD(multiplyIEEE_OneByIntegerOnlyFloat_ReturnsFloat)
		{
			// Arrange:
			const auto value = 2.0f;
			const auto one = 1.0f;

			// Act:
			const auto result = multiply_ieee(one, value);

			// Assert:
			Assert::AreEqual(value, result, L"multiply_ieee should have returned the float value, after attempting to multiply it by one");
		}

		TEST_METHOD(multiplyIEEE_IntegerOnlyFloats_ReturnsTheCorrectResult)
		{
			// Arrange:
			const auto left = 7.0f;
			const auto right = 19.0f;
			const auto expected = left * right;

			// Act:
			const auto result = multiply_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"multiply_ieee should have returned the expected result after multiplying two valid integer floats");
		}

		TEST_METHOD(multiplyIEEE_OneByFractionalOnlyFloat_ReturnsFloat)
		{
			// Arrange:
			const auto value = 0.7f;
			const auto one = 1.0f;

			// Act:
			const auto result = multiply_ieee(one, value);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(value, result), L"multiply_ieee should have returned the fractional float value, after attempting to multiply it by one");
		}

		TEST_METHOD(multiplyIEEE_SimpleFractionalOnlyFloats_ReturnsTheCorrectResult)
		{
			// Arrange:
			const auto left = 0.25f;
			const auto right = 0.5f;
			const auto expected = left * right;

			// Act:
			const auto result = multiply_ieee(left, right);

			// Assert:
			Assert::AreEqual(expected, result, L"multiply_ieee should have returned the expected result after multiplying two valid fractional floats");
		}

		TEST_METHOD(multiplyIEEE_ComplexFractionalOnlyFloats_ReturnsTheCorrectResult)
		{
			// Arrange:
			const auto left = 0.138f;
			const auto right = 0.261f;
			const auto expected = left * right;

			// Act:
			const auto result = multiply_ieee(left, right);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(expected, result), L"multiply_ieee should have returned the expected result after multiplying two valid fractional floats");
		}

		TEST_METHOD(multiplyIEEE_DifferentSignFloats_ReturnsANegativeFloat)
		{
			// Arrange:
			const auto left = 2.95f;
			const auto right = -9.8725f;
			const auto expected = left * right;

			// Act:
			const auto result = multiply_ieee(left, right);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(expected, result, MaxErrorTolerance), L"multiply_ieee should have returned a negative float after multiplying a negative and positive float");
			Assert::IsTrue(result < 0.0f, L"multiply_ieee should have returned the a negative float after multiplying a negative and positive float");
		}

		TEST_METHOD(multiplyIEEE_IntegerPartsLargerThanFractionalParts_ReturnsTheCorrectResult)
		{
			// Arrange:
			const auto left = 16384.5f;
			const auto right = 1024.5f;
			const auto expected = left * right;

			// Act:
			const auto result = multiply_ieee(left, right);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(expected, result, MaxErrorTolerance), L"multiply_ieee should have returned the expected result after multiplying two valid fractional floats");
		}

		TEST_METHOD(multiplyIEEE_BlogExample_ReturnsTheCorrectResult)
		{
			// Arrange:
			const auto left = 14.625f;
			const auto right = 2.5f;
			const auto expected = left * right;

			// Act:
			const auto result = multiply_ieee(left, right);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(expected, result, MaxErrorTolerance), L"multiply_ieee should have returned the expected result after multiplying two valid fractional floats");
		}

		TEST_METHOD(divideIEEE_DivideByZero_ReturnsFalse)
		{
			// Arrange:
			const auto numerator = 1.0f;
			const auto zero = 0.0f;
			auto result = 0.0f;

			// Act:
			const auto divisionSuccessful = divide_ieee(numerator, zero, result);

			// Assert:
			Assert::IsFalse(divisionSuccessful, L"divide_ieee should have been unsuccessful for zero denominator");
		}

		TEST_METHOD(divideIEEE_ZeroByValue_ReturnsZero)
		{
			// Arrange:
			const auto denominator = 1.0f;
			const auto zero = 0.0f;
			auto result = 1.0f;

			// Act:
			divide_ieee(zero, denominator, result);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(zero, result), L"divide_ieee should have returned zero for a zero numerator");
		}

		TEST_METHOD(divideIEEE_OneByIntegerFloat_ReturnsCorrectFraction)
		{
			// Arrange:
			const auto numerator = 1.0f;
			const auto denominator = 4.0f;
			const auto expected = numerator / denominator;
			auto result = 0.0f;

			// Act:
			divide_ieee(numerator, denominator, result);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(expected, result, MaxErrorTolerance), L"divide_ieee should have returned the correct fraction after dividing one by a float");
		}

		TEST_METHOD(divideIEEE_OneByFractionFloat_ReturnsCorrectIntegerFloat)
		{
			// Arrange:
			const auto numerator = 1.0f;
			const auto denominator = 0.25f;
			const auto expected = numerator / denominator;
			float result = 0.0f;

			// Act:
			divide_ieee(numerator, denominator, result);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(expected, result), L"divide_ieee should have returned the correct float after dividing one by a fraction");
		}

		TEST_METHOD(divideIEEE_FloatByOne_ReturnsFloat)
		{
			// Arrange:
			const auto numerator = 93.56f;
			const auto denominator = 1.0f;
			float result = 0.0f;

			// Act:
			divide_ieee(numerator, denominator, result);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(numerator, result), L"divide_ieee should have returned the correct float after dividing one by a fraction");
		}

		TEST_METHOD(divideIEEE_FloatByIntegerFloat_ReturnsCorrectResult)
		{
			// Arrange:
			const auto numerator = 11.25f;
			const auto denominator = 3.0f;
			const auto expected = numerator / denominator;
			float result = 0.0f;

			// Act:
			divide_ieee(numerator, denominator, result);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(expected, result, MaxErrorTolerance), L"divide_ieee should have returned the correct result after dividing a float by an integer float");
		}

		TEST_METHOD(divideIEEE_FloatByIrrationalFractionFloat_ReturnsValueGreaterThanFloat)
		{
			// Arrange:
			const auto numerator = 12.125f;
			const auto denominator = 0.2f;
			const auto expected = numerator / denominator;
			float result = 0.0f;

			// Act:
			divide_ieee(numerator, denominator, result);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(expected, result, MaxErrorTolerance), L"divide_ieee should have returned the correct result after dividing a float by another");
		}

		TEST_METHOD(divideIEEE_IrrationalFloatByIrrationalFloat_ReturnsCorrectResult)
		{
			// Arrange:
			const auto numerator = 17.2f;
			const auto denominator = 3.25f;
			const auto expected = numerator / denominator;
			float result = 0.0f;

			// Act:
			divide_ieee(numerator, denominator, result);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(expected, result, MaxErrorTolerance), L"divide_ieee should have returned the correct result after dividing a float by another");
		}

		TEST_METHOD(divideIEEE_BlogExample_ReturnsTheCorrectResult)
		{
			// Arrange:
			const auto numerator = 14.625f;
			const auto denominator = 2.5f;
			const auto expected = numerator / denominator;
			float result = 0.0f;

			// Act:
			divide_ieee(numerator, denominator, result);

			// Assert:
			Assert::IsTrue(AreAlmostEqual(expected, result, MaxErrorTolerance), L"divide_ieee should have returned the correct result after dividing a float by another");
		}
	};
}