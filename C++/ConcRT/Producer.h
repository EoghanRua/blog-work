#pragma once

#include <agents.h>
#include "ConcurrentQueue.h"
#include <concurrent_queue.h>
#include "IProducer.h"
#include "Thread.h"

namespace ConcRT
{
	class __declspec(dllexport) STLProducer final : public Thread, virtual public IProducer
	{
	public:
		explicit
		STLProducer(ConcurrentQueue<std::string>& queue);
		~STLProducer();

		void produce(const std::string& stringToEncode) override;
		void stop() override;

	protected:
		void run() override;

	private:
		ConcurrentQueue<std::string>& m_target;
		ConcurrentQueue<std::string> m_stringsToEncode;

		STLProducer(STLProducer&) = delete;
		STLProducer(STLProducer&&) = delete;
		STLProducer& operator=(STLProducer&) = delete;
		STLProducer& operator=(STLProducer&&) = delete;
	};

	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------

	class __declspec(dllexport) AALProducer final : public Concurrency::agent, virtual public IProducer
	{
	public:
		explicit
		AALProducer(Concurrency::ITarget<std::string>& targetBuffer);
		~AALProducer();

		void produce(const std::string& stringToEncode) override;
		void stop() override;

	protected:
		void run() override;

	private:
		Concurrency::ITarget<std::string>& m_targetBuffer;
		Concurrency::single_assignment<std::string> m_stopBuffer;
		Concurrency::concurrent_queue<std::string> m_stringsToEncode;

		AALProducer(AALProducer&) = delete;
		AALProducer(AALProducer&&) = delete;
		AALProducer& operator=(AALProducer&) = delete;
		AALProducer& operator=(AALProducer&&) = delete;
	};
}