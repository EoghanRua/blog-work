#pragma once

#include <memory>
#include <thread>

namespace ConcRT
{
	class __declspec(dllexport) Thread
	{
	public:
		Thread();
		virtual ~Thread();

		// Subsequent calls to start after the first have no effect and return false, until join is called
		bool start();
		bool join();

	protected:
		virtual void run() = 0;

	private:
		Thread(Thread&) = delete;
		Thread(Thread&&) = delete;
		Thread& operator=(Thread&) = delete;
		Thread& operator=(Thread&&) = delete;

		std::unique_ptr<std::thread> m_thread;
		bool m_canStart;
	};
}