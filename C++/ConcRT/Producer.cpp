#include "PCH.h"
#include "Producer.h"
#include "RLE.h"
#include <thread>

using namespace Concurrency;
using namespace std;

namespace ConcRT
{
	STLProducer::STLProducer(ConcurrentQueue<string>& queue)
		: Thread()
		, m_target(queue)
	{}

	STLProducer::~STLProducer()
	{
		stop();
	}

	void STLProducer::produce(const string& stringToEncode)
	{
		if (!stringToEncode.empty())
		{
			m_stringsToEncode.push(stringToEncode);
			start();
		}
	}

	void STLProducer::stop()
	{
		m_stringsToEncode.push(EndMessage);
		join();
	}

	void STLProducer::run()
	{
		string stringToEncode;

		// Blocking call - producer needs to populate the queue with a string to encode
		while ((stringToEncode = m_stringsToEncode.pop()) != EndMessage)
		{
			const auto encodedString = RLE::encode<string>(stringToEncode);
			m_target.push(encodedString);
		}
	}

	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------
	// -----------------------------------------------------------------------

	AALProducer::AALProducer(ITarget<std::string>& targetBuffer)
		: agent()
		, m_stopBuffer()
		, m_targetBuffer(targetBuffer)
		, m_stringsToEncode()
	{}

	AALProducer::~AALProducer()
	{
		stop();
	}

	void AALProducer::produce(const std::string& stringToEncode)
	{
		if (!stringToEncode.empty())
		{
			m_stringsToEncode.push(stringToEncode);

			// The agent only needs to start after the first call to 'produce'
			if (agent_status::agent_created == status())
			{
				start();
			}
		}
	}

	void AALProducer::stop()
	{
		// Inform the producer to stop
		asend(m_stopBuffer, EndMessage);

		// Blocking call - wait until the producer is 'done' after receiving the stop message
		if (agent_status::agent_runnable == status() || agent_status::agent_started == status())
		{
			agent::wait(this);
		}
	}

	void AALProducer::run()
	{
		string message;
		string stringToEncode;

		// Order of checks is important. Only check the 'stop' buffer after
		// checking the queue of strings to encode.
		while (true)
		{
			// Non-blocking call - data is only sent when it is available
			if (m_stringsToEncode.try_pop(stringToEncode))
			{
				const auto encodedString = RLE::encode<string>(stringToEncode);
				asend(m_targetBuffer, encodedString);
			}

			// Non-blocking call - check if a 'stop' has been requested
			if (try_receive(m_stopBuffer, message))
			{
				break;
			}
		}

		// Inform any potential consumers that we have finished producing
		asend(m_targetBuffer, EndMessage);

		done();
	}
}