#pragma once

namespace ConcRT
{
	class __declspec(dllexport) IConsumer
	{
	public:
		virtual ~IConsumer(){}
		virtual bool consume(std::string& consumedData) = 0;
		virtual bool consume(std::string& consumedData, unsigned int timeout) = 0;
		virtual void stop() = 0;
	};
}