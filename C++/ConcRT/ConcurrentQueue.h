#pragma once

#include <chrono>
#include <condition_variable>
#include <mutex>
#include <queue>

namespace ConcRT
{
	template <typename T, typename ContainerClass = std::deque<T>>
	class __declspec(dllexport) ConcurrentQueue final
	{
	public:
		ConcurrentQueue();

		explicit
		ConcurrentQueue(const std::queue<T, ContainerClass>& items);

		void push(const T& item);
		// Returns a flag specifying success or failure (empty queue) as well as assigning to the reference 
		bool try_pop(T& item);
		// Same as above but polls the queue until the timeout expires
		bool try_pop(T& item, std::size_t timeout);
		// Blocks until the queue is populated through the use of a condition variable
		T pop();
		bool empty();
		std::size_t size();

		// Used for testing purposes
		std::mutex& getMutex();

	private:
		std::mutex m_mutex;
		std::queue<T, ContainerClass> m_items;
		std::condition_variable m_cv;

		ConcurrentQueue(ConcurrentQueue&) = delete;
		ConcurrentQueue(ConcurrentQueue&&) = delete;
		ConcurrentQueue& operator=(ConcurrentQueue&) = delete;
		ConcurrentQueue& operator=(ConcurrentQueue&&) = delete;

		bool popInternal(T& item);
	};
}

#include "ConcurrentQueue.inl"