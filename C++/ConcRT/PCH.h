#include <string>
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>

// I can safely ignore this warning because I am sure that there will be no difference between
// the target platform for this dll the target platform for the dependent console application.
// Therefore I know that the implementation of any STL class will be the same
#pragma warning (disable : 4251)

// I can safely ignore this warning because I am sure that there will be no difference between
// the target platform used by the base class 'Concurrency::agent' and the target platform used by the derived
// class since they are both in the same solution
#pragma warning (disable : 4275)

static std::string EndMessage = "End";