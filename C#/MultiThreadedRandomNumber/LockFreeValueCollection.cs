﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MultiThreadedRandomNumber
{
    public sealed class LockFreeValueCollection<T> : IValueCollection<T>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        public LockFreeValueCollection()
        {
            mValues = new HashSet<T>();
        }

        public bool AddUnique(T value)
        {
            if (!mValues.Add(value))
                return false;

            return true;
        }

        public bool Remove(int index, ref T value)
        {
            if (index < 0 || index >= Count)
                return false;

            value = mValues.ElementAt(index);
            return true;
        }

        public void Clear()
        {
            mValues.Clear();
        }

        public IEnumerable<T> GetValueEnumerable()
        {
            return mValues;
        }
        
        public int Count { get { return mValues.Count; } }

        private HashSet<T> mValues;
    };
}
