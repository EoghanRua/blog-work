﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace MultiThreadedRandomNumber
{
    public sealed class LockingValueCollection<T> : IValueCollection<T>
        where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
    {
        public LockingValueCollection()
        {
            mValues = new ConcurrentDictionary<T, T>();
        }

        public bool AddUnique(T value)
        {
            if (mValues.Keys.Contains(value))
                return false;

            mValues.TryAdd(value, value);
            return true;
        }

        public bool Remove(int index, ref T value)
        {
            if (index < 0 || index >= Count)
                return false;

            return mValues.TryRemove(mValues.ElementAt(index).Key, out value); 
        }

        public void Clear()
        {
            mValues.Clear();
        }

        public IEnumerable<T> GetValueEnumerable()
        {
            return mValues.Values;
        }

        public int Count { get { return mValues.Count; } }

        // I decided to use the .Net thread safe implementation of a dictionary as it uses fine grained locking
        // for write operations and is lock free for read operations
        // Can't use a ConcurrentBag here because I need the ability to remove a value at a specific index
        private ConcurrentDictionary<T, T> mValues;
    };
}
