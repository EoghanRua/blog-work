﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreadedRandomNumber
{
    public static class RandomNumber
    {
        public enum ThreadingMethod
        {
            BackgroundWorker,
            Task,
            Thread,
            ThreadPool,
            TAP,
            Timer
        }

        public enum SynchronisationMethod
        {
            Callback,
            Locking,
            Signalling
        }

        public static IValueCollection<T> CreateCollection<T>(bool locking)
            where T : struct, IComparable, IFormattable, IConvertible, IComparable<T>, IEquatable<T>
        {
            if (locking)
            {
                return new LockingValueCollection<T>();
            }
            else
            {
                return new LockFreeValueCollection<T>();
            }
        }

        public static IThreadingMethod CreateThreadingMethod(ThreadingMethod tMethod, SynchronisationMethod sMethod)
        {
            switch (tMethod)
            {
                case ThreadingMethod.BackgroundWorker:
                {
                    switch (sMethod)
                    {
                        case SynchronisationMethod.Callback: return new CallbackBackgroundWorker();
                        case SynchronisationMethod.Locking: return new LockingBackgroundWorker();
                        case SynchronisationMethod.Signalling: return new SignallingBackgroundWorker();
                    }
                }
                break;

                case ThreadingMethod.Task:
                {
                    switch (sMethod)
                    {
                        case SynchronisationMethod.Callback: return new CallbackTask();
                        case SynchronisationMethod.Locking: return new LockingTask();
                        case SynchronisationMethod.Signalling: return new SignallingTask();
                    }
                }
                break;

                case ThreadingMethod.Thread:
                {
                    switch (sMethod)
                    {
                        case SynchronisationMethod.Callback: return new CallbackThread();
                        case SynchronisationMethod.Locking: return new LockingThread();
                        case SynchronisationMethod.Signalling: return new SignallingThread();
                    }
                }
                break;

                case ThreadingMethod.ThreadPool:
                {
                    switch (sMethod)
                    {
                        case SynchronisationMethod.Locking: return new LockingThreadPool();
                        case SynchronisationMethod.Callback: return new CallbackThreadPool();
                        case SynchronisationMethod.Signalling: return new SignallingThreadPool();
                    }
                }
                break;

                case ThreadingMethod.TAP:
                {
                    switch (sMethod)
                    {
                        case SynchronisationMethod.Callback: return new TaskAsyncPattern();
                    }
                }
                break;
            }

            return null;
        }

        // From Joe Albahari:
        /*
         * http://www.albahari.com/threading/part2.aspx#_Signaling_with_Event_Wait_Handles
         * 
         * Once you’ve finished with a wait handle, you can call its Close method to release the operating
         * system resource. Alternatively, you can simply drop all references to the wait handle and allow
         * the garbage collector to do the job for you sometime later (wait handles implement the disposal
         * pattern whereby the finalizer calls Close). This is one of the few scenarios where relying on
         * this backup is (arguably) acceptable, because wait handles have a light OS burden (asynchronous
         * delegates rely on exactly this mechanism to release their IAsyncResult’s wait handle).  Wait
         * handles are released automatically when an application domain unloads.
         */
        public static void DisposeThreadingMethod(IThreadingMethod threadingMethod)
        {
            //if (threadingMethod is IDisposable)
            //{
            //    var disposableTM = threadingMethod as IDisposable;
            //    disposableTM.Dispose();
            //}
        }

        public static void Populate(IValueCollection<uint> valueCollection, Random randomNumberGenerator)
        {
            // Generate a unique random number, between 1 and 100, to add to the collection
            if (valueCollection.Count < 100)
            {
                uint newRandomNumber = Convert.ToUInt32(randomNumberGenerator.Next(1, 100));
                while (valueCollection.GetValueEnumerable().Contains(newRandomNumber))
                {
                    newRandomNumber = Convert.ToUInt32(randomNumberGenerator.Next(1, 100));
                }

                valueCollection.AddUnique(newRandomNumber);

                Console.WriteLine("ThreadID {0} Populate", Thread.CurrentThread.ManagedThreadId);
            }
        }

        public static void Update(IValueCollection<uint> valueCollection, Random randomNumberGenerator)
        {
            if (valueCollection.Count > 0)
            {
                // Identify a random number to remove from the container and square it
                int randomNumberIndex = randomNumberGenerator.Next(0, valueCollection.Count - 1);
                uint randomNumberRemoved = 0;
                if (valueCollection.Remove(randomNumberIndex, ref randomNumberRemoved))
                {
                    ulong numberSquared = randomNumberRemoved * randomNumberRemoved;

                    // Only add numbers to the collection that are in the range for 32-bit integers
                    if (numberSquared > uint.MinValue && numberSquared < uint.MaxValue)
                    {
                        uint newNumber = Convert.ToUInt32(numberSquared);
                        valueCollection.AddUnique(newNumber);
                    }
                    else
                    {
                        Console.WriteLine("ERROR: New number isn't in the valid range so ignoring it {0}", numberSquared);
                    }
                }
                else
                {
                    Console.WriteLine("ERROR: Failed to remove the number at index {0}. Container count: {1}", randomNumberIndex, valueCollection.Count);
                }

                Console.WriteLine("ThreadID {0} Update", Thread.CurrentThread.ManagedThreadId);
            }
            else
            {
                throw new InvalidOperationException("The value collection is empty");
            }
        }

        public static void PrintSum(IValueCollection<uint> valueCollection)
        {
            long sum = valueCollection.GetValueEnumerable().Sum(x => Convert.ToInt64(x));
            if (sum > uint.MaxValue)
            {
                valueCollection.Clear();
                Console.WriteLine("Sum of numbers is too large so clearing the container");
            }
            else
            {
                Console.WriteLine("Sum of all random numbers: {0}", sum);
            }

            Console.WriteLine("ThreadID {0} PrintSum", Thread.CurrentThread.ManagedThreadId);
        }
    }
}
