﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadedRandomNumber
{
    public sealed class TaskAsyncPattern : IThreadingMethod
    {
        public async void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time)
        {
            if (mCurrentTask != null)
                await mCurrentTask;

            var populate = Task.Run(() => populateAction());
            mCurrentTask = populate;
            await populate;

            if (time.Seconds % 2 == 0)
            {
                var update = Task.Run(() => updateAction());
                mCurrentTask = update;
                await update;
            }

            if (time.Seconds % 3 == 0)
            {
                var printSum = Task.Run(() => printSumAction());
                mCurrentTask = printSum;
                await printSum;
            }
        }

        private Task mCurrentTask;
    }
}
