﻿using System;
using System.Threading;
using System.Runtime.InteropServices;

namespace MultiThreadedRandomNumber
{
    public sealed class CallbackThread : IThreadingMethod
    {
        public void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time)
        {
            if (mCurrentThread != null)
                mCurrentThread.Join();
            var populate = new Thread(() => populateAction());
            populate.Start();
            mCurrentThread = populate;

            if (time.Seconds % 2 == 0)
            {
                mCurrentThread.Join();
                var update = new Thread(() => updateAction());
                update.Start();
                mCurrentThread = update;
            }

            if (time.Seconds % 3 == 0)
            {
                mCurrentThread.Join();
                var printSum = new Thread(() => printSumAction());
                printSum.Start();
                mCurrentThread = printSum;
            }
        }

        private Thread mCurrentThread;
    }
}
