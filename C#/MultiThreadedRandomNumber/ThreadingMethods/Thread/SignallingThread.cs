﻿using System;
using System.Threading;

namespace MultiThreadedRandomNumber
{
    public sealed class SignallingThread : SignallingThreadingMethod
    {
        protected override void BeginCollectionPopulate(Action populateAction, Action signallingAction)
        {
            var populate = new Thread(() => { populateAction(); signallingAction(); });
            populate.Start();
        }

        protected override void BeginCollectionUpdate(Action updateAction, Action signallingAction)
        {
            var update = new Thread(() => { updateAction(); signallingAction(); });
            update.Start();
        }

        protected override void BeginCollectionPrintSum(Action printSumAction, Action signallingAction)
        {
            var printSum = new Thread(() => { printSumAction(); signallingAction(); });
            printSum.Start();
        }
    }
}
