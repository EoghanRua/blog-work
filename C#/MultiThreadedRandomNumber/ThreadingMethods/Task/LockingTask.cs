﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiThreadedRandomNumber
{
    public sealed class LockingTask : IThreadingMethod
    {
        public void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time)
        {
            Task.Run(() => populateAction());

            if(time.Seconds % 2 == 0)
                Task.Run(() => updateAction());

            if(time.Seconds % 3 == 0)
                Task.Run(() => printSumAction());
        }
    }
}
