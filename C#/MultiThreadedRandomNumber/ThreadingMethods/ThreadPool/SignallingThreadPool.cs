﻿using System;
using System.Threading;

namespace MultiThreadedRandomNumber
{
    public sealed class SignallingThreadPool : SignallingThreadingMethod
    {
        protected override void BeginCollectionPopulate(Action populateAction, Action signallingAction)
        {
            ThreadPool.QueueUserWorkItem((o) => { populateAction(); signallingAction(); });
        }

        protected override void BeginCollectionUpdate(Action updateAction, Action signallingAction)
        {
            ThreadPool.QueueUserWorkItem((o) => { updateAction(); signallingAction(); });
        }

        protected override void BeginCollectionPrintSum(Action printSumAction, Action signallingAction)
        {
            ThreadPool.QueueUserWorkItem((o) => { printSumAction(); signallingAction(); });
        }
    }
}
