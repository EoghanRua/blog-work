﻿using System;
using System.Threading;

namespace MultiThreadedRandomNumber
{
    public sealed class LockingThreadPool : IThreadingMethod
    {
        public void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time)
        {
            ThreadPool.QueueUserWorkItem((o) => populateAction());

            if(time.Seconds % 2 == 0)
                ThreadPool.QueueUserWorkItem((o) => updateAction());

            if(time.Seconds % 3 == 0)
                ThreadPool.QueueUserWorkItem((o) => printSumAction());
        }
    }
}