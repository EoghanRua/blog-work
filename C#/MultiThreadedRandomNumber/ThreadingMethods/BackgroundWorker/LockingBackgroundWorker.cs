﻿using System;
using System.ComponentModel;

namespace MultiThreadedRandomNumber
{
    public sealed class LockingBackgroundWorker : IThreadingMethod
    {
        public void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time)
        {
            var populate = new BackgroundWorker();
            populate.DoWork += (dwO, dwE) => populateAction();
            populate.RunWorkerAsync();

            if (time.Seconds % 2 == 0)
            {
                var update = new BackgroundWorker();
                update.DoWork += (dwO, dwE) => updateAction();
                update.RunWorkerAsync();
            }

            if (time.Seconds % 3 == 0)
            {
                var printSum = new BackgroundWorker();
                printSum.DoWork += (dwO, dwE) => printSumAction();
                printSum.RunWorkerAsync();
            }
        }
    }
}
