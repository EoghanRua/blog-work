﻿using System;
using System.Threading;

namespace MultiThreadedRandomNumber
{
    public abstract class SignallingThreadingMethod : IThreadingMethod, IDisposable
    {
        public void ExecuteCollectionActions(
            Action populateAction,
            Action updateAction,
            Action printSumAction,
            TimeSpan time)
        {
            if (mEventToWaitFor != null)
                mEventToWaitFor.WaitOne();
            BeginCollectionPopulate(populateAction, () => mPopulate.Set());
            mEventToWaitFor = mPopulate;

            if (time.Seconds % 2 == 0)
            {
                mEventToWaitFor.WaitOne();
                BeginCollectionUpdate(updateAction, () => mUpdate.Set());
                mEventToWaitFor = mUpdate;
            }

            if (time.Seconds % 3 == 0)
            {
                mEventToWaitFor.WaitOne();
                BeginCollectionPrintSum(printSumAction, () => mPrintSum.Set());
                mEventToWaitFor = mPrintSum;
            }
        }

        public void Dispose()
        {
            mPopulate.Dispose();
            mUpdate.Dispose();
            mPrintSum.Dispose();
        }

        protected abstract void BeginCollectionPopulate(Action populateAction, Action signallingAction);
        protected abstract void BeginCollectionUpdate(Action updateAction, Action signallingAction);
        protected abstract void BeginCollectionPrintSum(Action printSumAction, Action signallingAction);

        private AutoResetEvent mPopulate = new AutoResetEvent(false);
        private AutoResetEvent mUpdate = new AutoResetEvent(false);
        private AutoResetEvent mPrintSum = new AutoResetEvent(false);
        private AutoResetEvent mEventToWaitFor = null;
    }
}
