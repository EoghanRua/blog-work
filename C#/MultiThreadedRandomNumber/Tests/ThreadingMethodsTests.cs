﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Extensions;

// Limitations:
// 1)   Using a locking container for the Locking threading method don't enforce an order of
//      thread execution and therefore we can't validate the expected order

namespace MultiThreadedRandomNumber.Tests
{
    using sm = RandomNumber.SynchronisationMethod;
    using tm = RandomNumber.ThreadingMethod;

    public class ThreadingMethodsTests
    {
        [Theory]
        [InlineData(tm.BackgroundWorker, sm.Callback)]
        [InlineData(tm.BackgroundWorker, sm.Locking)]
        [InlineData(tm.BackgroundWorker, sm.Signalling)]
        [InlineData(tm.Task, sm.Callback)]
        [InlineData(tm.Task, sm.Locking)]
        [InlineData(tm.Task, sm.Signalling)]
        [InlineData(tm.Thread, sm.Callback)]
        [InlineData(tm.Thread, sm.Locking)]
        [InlineData(tm.Thread, sm.Signalling)]
        [InlineData(tm.ThreadPool, sm.Callback)]
        [InlineData(tm.ThreadPool, sm.Locking)]
        [InlineData(tm.ThreadPool, sm.Signalling)]
        [InlineData(tm.TAP, sm.Callback)]
        public void populate_inserts_expected_value_in_value_collection(tm tMethod, sm sMethod)
        {
            // Arrange:
            var valueCollection = RandomNumber.CreateCollection<uint>(sMethod == sm.Locking);
            var threadingMethod = RandomNumber.CreateThreadingMethod(tMethod, sMethod);
            uint randomNumberToProduce = 5;
            var generator = new FakeRandomNumberGenerator(Convert.ToInt32(randomNumberToProduce));
            var oneSecond = TimeSpan.FromSeconds(1.0);

            // Act:
            using (var eventToWaitFor = new AutoResetEvent(false))
            {
                threadingMethod.ExecuteCollectionActions(
                    () => { RandomNumber.Populate(valueCollection, generator); eventToWaitFor.Set(); },
                    null,
                    null,
                    oneSecond);
                eventToWaitFor.WaitOne();
            }

            // Assert:
            Assert.Contains(randomNumberToProduce, valueCollection.GetValueEnumerable());
            Assert.True(valueCollection.GetValueEnumerable().Count() == 1);

            RandomNumber.DisposeThreadingMethod(threadingMethod);
        }

        [Theory]
        [InlineData(tm.BackgroundWorker, sm.Callback)]
        [InlineData(tm.BackgroundWorker, sm.Locking)]
        [InlineData(tm.BackgroundWorker, sm.Signalling)]
        [InlineData(tm.Task, sm.Callback)]
        [InlineData(tm.Task, sm.Locking)]
        [InlineData(tm.Task, sm.Signalling)]
        [InlineData(tm.Thread, sm.Callback)]
        [InlineData(tm.Thread, sm.Locking)]
        [InlineData(tm.Thread, sm.Signalling)]
        [InlineData(tm.ThreadPool, sm.Callback)]
        [InlineData(tm.ThreadPool, sm.Locking)]
        [InlineData(tm.ThreadPool, sm.Signalling)]
        [InlineData(tm.TAP, sm.Callback)]
        public void update_throws_invalid_operation_exception_for_empty_value_collection(tm tMethod, sm sMethod)
        {
            // Arrange:
            var valueCollection = RandomNumber.CreateCollection<uint>(sMethod == sm.Locking);
            var threadingMethod = RandomNumber.CreateThreadingMethod(tMethod, sMethod);
            var twoSeconds = TimeSpan.FromSeconds(2.0);

            // Act:
            // Assert:
            threadingMethod.ExecuteCollectionActions(
                () => { },
                () => Assert.Throws<InvalidOperationException>(() => RandomNumber.Update(valueCollection, null)),
                () => { },
                twoSeconds);

            RandomNumber.DisposeThreadingMethod(threadingMethod);
        }

        [Theory]
        [InlineData(tm.BackgroundWorker, sm.Callback)]
        //[InlineData(tm.BackgroundWorker, sm.Locking)]
        [InlineData(tm.BackgroundWorker, sm.Signalling)]
        [InlineData(tm.Task, sm.Callback)]
        //[InlineData(tm.Task, sm.Locking)]
        [InlineData(tm.Task, sm.Signalling)]
        [InlineData(tm.Thread, sm.Callback)]
        //[InlineData(tm.Thread, sm.Locking)]
        [InlineData(tm.Thread, sm.Signalling)]
        [InlineData(tm.ThreadPool, sm.Callback)]
        //[InlineData(tm.ThreadPool, sm.Locking)]
        [InlineData(tm.ThreadPool, sm.Signalling)]
        [InlineData(tm.TAP, sm.Callback)]
        public void update_produces_25_from_value_collection_containing_5(tm tMethod, sm sMethod)
        {
            // Arrange:
            var valueCollection = RandomNumber.CreateCollection<uint>(sMethod == sm.Locking);
            var threadingMethod = RandomNumber.CreateThreadingMethod(tMethod, sMethod);
            int randomNumberToProduce = 5;
            int[] randomNumbers = { randomNumberToProduce, 0 };
            var generator = new FakeRandomNumberGenerator(randomNumbers);
            var twoSeconds = TimeSpan.FromSeconds(2.0);

            // Act:
            using (var eventToWaitFor = new AutoResetEvent(false))
            {
                threadingMethod.ExecuteCollectionActions(
                    () => RandomNumber.Populate(valueCollection, generator),
                    () => { RandomNumber.Update(valueCollection, generator); eventToWaitFor.Set(); },
                    null,
                    twoSeconds);
                eventToWaitFor.WaitOne();
            }

            // Assert:
            var expected = randomNumberToProduce * randomNumberToProduce;
            Assert.True(valueCollection.GetValueEnumerable().Count() == 1);
            Assert.Contains(Convert.ToUInt32(expected), valueCollection.GetValueEnumerable());

            RandomNumber.DisposeThreadingMethod(threadingMethod);
        }
                
        [Theory]
        [InlineData(tm.BackgroundWorker, sm.Callback)]
        //[InlineData(tm.BackgroundWorker, sm.Locking)]
        [InlineData(tm.BackgroundWorker, sm.Signalling)]
        [InlineData(tm.Task, sm.Callback)]
        //[InlineData(tm.Task, sm.Locking)]
        [InlineData(tm.Task, sm.Signalling)]
        [InlineData(tm.Thread, sm.Callback)]
        //[InlineData(tm.Thread, sm.Locking)]
        [InlineData(tm.Thread, sm.Signalling)]
        [InlineData(tm.ThreadPool, sm.Callback)]
        //[InlineData(tm.ThreadPool, sm.Locking)]
        [InlineData(tm.ThreadPool, sm.Signalling)]
        [InlineData(tm.TAP, sm.Callback)]
        public void printsum_produces_expected_value_in_value_collection(tm tMethod, sm sMethod)
        {
            // Arrange:
            var valueCollection = RandomNumber.CreateCollection<uint>(sMethod == sm.Locking);
            var threadingMethod = RandomNumber.CreateThreadingMethod(tMethod, sMethod);
            int[] randomNumbers = { 1, 2, 3, 4 };
            var generator = new FakeRandomNumberGenerator(randomNumbers);
            int result = 0;
            int expected = randomNumbers.Sum();
            var threeSeconds = TimeSpan.FromSeconds(3.0);
                        
            // Act:
            // Equivalent to calling Populate 3 times
            valueCollection.AddUnique(Convert.ToUInt32(generator.Next(0, randomNumbers.Max())));
            valueCollection.AddUnique(Convert.ToUInt32(generator.Next(0, randomNumbers.Max())));
            valueCollection.AddUnique(Convert.ToUInt32(generator.Next(0, randomNumbers.Max())));

            using (var eventToWaitFor = new AutoResetEvent(false))
            {
                threadingMethod.ExecuteCollectionActions(
                    () => RandomNumber.Populate(valueCollection, generator),
                    null,
                    () => { result = valueCollection.GetValueEnumerable().Sum((x) => Convert.ToInt32(x)); eventToWaitFor.Set(); },
                    threeSeconds);
                eventToWaitFor.WaitOne();
            }

            // Assert:
            Assert.Equal(expected, result);

            RandomNumber.DisposeThreadingMethod(threadingMethod);
        }

        [Theory]
        [InlineData(tm.BackgroundWorker, sm.Callback)]
        //[InlineData(tm.BackgroundWorker, sm.Locking)]
        [InlineData(tm.BackgroundWorker, sm.Signalling)]
        [InlineData(tm.Task, sm.Callback)]
        //[InlineData(tm.Task, sm.Locking)]
        [InlineData(tm.Task, sm.Signalling)]
        [InlineData(tm.Thread, sm.Callback)]
        //[InlineData(tm.Thread, sm.Locking)]
        [InlineData(tm.Thread, sm.Signalling)]
        [InlineData(tm.ThreadPool, sm.Callback)]
        //[InlineData(tm.ThreadPool, sm.Locking)]
        [InlineData(tm.ThreadPool, sm.Signalling)]
        [InlineData(tm.TAP, sm.Callback)]
        public void executing_collection_actions_multiple_times_produces_expected_sum(tm tMethod, sm sMethod)
        {
            // Arrange:
            var valueCollection = RandomNumber.CreateCollection<uint>(sMethod == sm.Locking);
            var threadingMethod = RandomNumber.CreateThreadingMethod(tMethod, sMethod);

            // The order and value of these numbers is important for the purposes of this test
            // The locking value collection automatically sorts its contents so we need to account for that
            // by ensuring that squaring a particular value won't change the order
            int[] populateRandomNumbers = { 1, 2, 6 }; 
            var populateGenerator = new FakeRandomNumberGenerator(populateRandomNumbers);
            int[] updateRandomNumbers = { 0, 1, 2 };
            var updateGenerator = new FakeRandomNumberGenerator(updateRandomNumbers);
            int expected =  populateRandomNumbers.Sum((x) => x * x);
            var numLoops = populateRandomNumbers.Count();

            // Create an AutoResetEvent for each iteration so we can check that
            // each action has completed before validating the results
            // TODO_EÓF: Is there a better way of doing this?
            var events = new List<AutoResetEvent>(numLoops);
            for (int i = 0; i < numLoops; ++i)
                events.Add(new AutoResetEvent(false));
            var eventEnumerator = events.GetEnumerator();
            eventEnumerator.MoveNext();

            // Act:
            for (int i = 0; i < numLoops; ++i)
            {
                threadingMethod.ExecuteCollectionActions(
                    () => RandomNumber.Populate(valueCollection, populateGenerator),
                    () => RandomNumber.Update(valueCollection, updateGenerator),
                    () => 
                    {
                        RandomNumber.PrintSum(valueCollection);
                        (eventEnumerator.Current as AutoResetEvent).Set();
                        eventEnumerator.MoveNext();
                    },
                    TimeSpan.Zero);
            }

            // We need to wait until after all the treads have completed before validating the collection contents
            foreach (var e in events)
                e.WaitOne();

            // Assert:
            int result = valueCollection.GetValueEnumerable().Sum((x) => Convert.ToInt32(x));
            Assert.Equal(expected, result);

            RandomNumber.DisposeThreadingMethod(threadingMethod);
        }

        [Theory]
        [InlineData(tm.BackgroundWorker, sm.Callback)]
        //[InlineData(tm.BackgroundWorker, sm.Locking)]
        [InlineData(tm.BackgroundWorker, sm.Signalling)]
        [InlineData(tm.Task, sm.Callback)]
        //[InlineData(tm.Task, sm.Locking)]
        [InlineData(tm.Task, sm.Signalling)]
        [InlineData(tm.Thread, sm.Callback)]
        //[InlineData(tm.Thread, sm.Locking)]
        [InlineData(tm.Thread, sm.Signalling)]
        [InlineData(tm.ThreadPool, sm.Callback)]
        //[InlineData(tm.ThreadPool, sm.Locking)]
        [InlineData(tm.ThreadPool, sm.Signalling)]
        [InlineData(tm.TAP, sm.Callback)]
        public void generate_timing_info_for_threading_methods(tm tMethod, sm sMethod)
        {
            // Arrange:
            var valueCollection = RandomNumber.CreateCollection<uint>(sMethod == sm.Locking);
            var threadingMethod = RandomNumber.CreateThreadingMethod(tMethod, sMethod);
            List<int> randomNumbers = new List<int>();
            const int numLoops = 5;

            // Create an AutoResetEvent for each iteration so we can check that
            // each action has completed before validating the results
            // TODO_EÓF: Is there a better way of doing this?
            var events = new List<AutoResetEvent>(numLoops);
            for (int i = 0; i < numLoops; ++i)
            {
                randomNumbers.Add(i);
                randomNumbers.Add(numLoops - i - 1);
                events.Add(new AutoResetEvent(false));
            }
            var eventEnumerator = events.GetEnumerator();
            eventEnumerator.MoveNext();

            // Random number generate will need to produce numLoops * 2 random numbers none of which can be greater than numLoops
            var generator = new FakeRandomNumberGenerator(randomNumbers.ToArray());
            var timer = new Stopwatch();

            // Act:
            timer.Start();
            for (int i = 0; i < numLoops; ++i)
            {
                threadingMethod.ExecuteCollectionActions(
                    () => RandomNumber.Populate(valueCollection, generator),
                    () => RandomNumber.Update(valueCollection, generator),
                    () =>
                    {
                        RandomNumber.PrintSum(valueCollection);
                        (eventEnumerator.Current as AutoResetEvent).Set();
                        eventEnumerator.MoveNext();
                    },
                    TimeSpan.Zero);
            }
            timer.Stop();

            // We need to wait until after all the treads have completed before validating the collection contents
            foreach (var e in events)
                e.WaitOne();

            // Assert:
            Console.WriteLine(string.Format("Threading Method: {0}, Synchronization Method: {1}", tMethod.ToString(), sMethod.ToString()));
            Console.WriteLine(string.Format("Total Time: {0}", timer.ElapsedMilliseconds));
            Console.WriteLine(string.Format("Average Time: {0}", timer.ElapsedMilliseconds / numLoops));

            RandomNumber.DisposeThreadingMethod(threadingMethod);
        }
    }
}
