﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.Concurrency.TestTools.UnitTesting;
using Microsoft.Concurrency.TestTools.UnitTesting.Chess;

namespace MultiThreadedRandomNumber.Tests
{
    // Note the following aren't supported by CHESS:
    //  - BackgroundWorker
    //  - Task and TaskFactory
    // Also:
    // Couldn't use TestArgs here because CHESS converts the parameters to string
    // before converting them back to the original type using 'Type.GetType'.  This
    // only works for types that are defined in mscorlib or the currently executing
    // assembly when the fully qualified type name isn't supplied.

    using sm = MultiThreadedRandomNumber.RandomNumber.SynchronisationMethod;
    using tm = MultiThreadedRandomNumber.RandomNumber.ThreadingMethod;

    public class ChessTests
    {
        [ChessTestMethod()]
        [ChessTestContext(PreemptAllAccesses = true, EnableAtomicityChecking = true, EnableRaceDetection = true,
            MaxExecTime = 5, MaxExecs = 1000, MaxPreemptions = 5)]
        public void CallbackThread_MultipleRuns_ProducesSumOfSampleRandomNumbers()
        {
            ExecuteAllActionsMultipleTimes(tm.Thread, sm.Callback);
        }

        [ChessTestMethod()]
        [ChessTestContext(PreemptAllAccesses = true, EnableAtomicityChecking = true, EnableRaceDetection = true,
            MaxExecTime = 5, MaxExecs = 1000, MaxPreemptions = 5)]
        public void SignallingThread_MultipleRuns_ProducesSumOfSampleRandomNumbers()
        {
            ExecuteAllActionsMultipleTimes(tm.Thread, sm.Signalling);
        }

        [ChessTestMethod()]
        [ScheduleTestMethod(MaxPreemptions=5, MaxRunTime=30, MaxSchedules=1000)]
        [ChessTestContext(PreemptAllAccesses = true, EnableAtomicityChecking = true, EnableRaceDetection = true,
            MaxExecTime = 5, MaxExecs = 1000, MaxPreemptions = 5)]
        public void CallbackThreadPool_MultipleRuns_ProducesSumOfSampleRandomNumbers()
        {
            ExecuteAllActionsMultipleTimes(tm.ThreadPool, sm.Callback);
        }

        [ChessTestMethod()]
        [ChessTestContext(PreemptAllAccesses = true, EnableAtomicityChecking = true, EnableRaceDetection = true,
            MaxExecTime = 5, MaxExecs = 1000, MaxPreemptions = 5)]
        public void SignallingThreadPool_MultipleRuns_ProducesSumOfSampleRandomNumbers()
        {
            ExecuteAllActionsMultipleTimes(tm.ThreadPool, sm.Signalling);
        }

        private void ExecuteAllActionsMultipleTimes(tm tMethod, sm sMethod)
        {
            // Arrange:
            var valueCollection = RandomNumber.CreateCollection<uint>(false);
            var threadingMethod = RandomNumber.CreateThreadingMethod(tMethod, sMethod);

            int[] populateRandomNumbers = { 1, 2, 6, 9 };
            var populateGenerator = new FakeRandomNumberGenerator(populateRandomNumbers);
            int[] updateRandomNumbers = { 0, 1, 2, 3 };
            var updateGenerator = new FakeRandomNumberGenerator(updateRandomNumbers);
            int expected = populateRandomNumbers.Sum((x) => x * x);
            var numLoops = populateRandomNumbers.Count();

            // Create an AutoResetEvent for each iteration so we can check that
            // each action has completed before validating the results
            var events = new List<AutoResetEvent>(numLoops);
            for (int i = 0; i < numLoops; ++i)
                events.Add(new AutoResetEvent(false));
            var eventEnumerator = events.GetEnumerator();
            eventEnumerator.MoveNext();

            // Act:
            for (int i = 0; i < numLoops; ++i)
            {
                threadingMethod.ExecuteCollectionActions(
                    () => RandomNumber.Populate(valueCollection, populateGenerator),
                    () => RandomNumber.Update(valueCollection, updateGenerator),
                    () =>
                    {
                        RandomNumber.PrintSum(valueCollection);
                        (eventEnumerator.Current as AutoResetEvent).Set();
                        eventEnumerator.MoveNext();
                    },
                    TimeSpan.Zero);
            }

            // We need to wait until after all the treads have completed before validating the collection contents
            foreach (var e in events)
                e.WaitOne();

            // Assert:
            int result = valueCollection.GetValueEnumerable().Sum((x) => Convert.ToInt32(x));
            Assert.AreEqual(expected, result);

            RandomNumber.DisposeThreadingMethod(threadingMethod);
        }
    }
}
