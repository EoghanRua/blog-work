﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScrollingAnimationsPrism.Services
{
    public interface IPanelLoaderService
    {
        Task<IEnumerable<PanelsPanel>> LoadPanels();
    }
}
