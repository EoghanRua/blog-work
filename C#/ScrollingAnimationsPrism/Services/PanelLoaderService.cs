﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ScrollingAnimationsPrism.Services
{
    public class PanelLoaderService : IPanelLoaderService
    {
        public string FilePath { get; set; }        

        public PanelLoaderService()
        {
            FilePath = "../../ScrollingAnimationsPrism/Data/scrolling_panels.xml";
        }

        public async Task<IEnumerable<PanelsPanel>> LoadPanels()
        {
            var serializer = new XmlSerializer(typeof(Panels));

            return await Task.Run(() =>
            {
                var fileStream = new FileStream(FilePath, FileMode.Open);
                var xmlReader = XmlReader.Create(fileStream);
                var scrollingPanels = serializer.Deserialize(xmlReader) as Panels;
                return scrollingPanels.Panel;
            });
        }
    }
}
