﻿using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ScrollingAnimationsPrism.Utiltilities;
using Xunit;

namespace ScrollingAnimationsPrism.Tests
{
    public class PanelsControl_Unit
    {
        [Fact]
        [Trait("Category", "Unit")]
        void Ctr_WithTwoImages_SetsOnScreenImageToFirstAndOffScreenImageToSecondImage()
        {
            var panels = CreatePanels("image1", "image2");
            var testSystemWrapper = new TestSystemWrapper();

            var control = CreatePanelsControl().WithPanels(panels).WithSystemWrapper(testSystemWrapper).Build();

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image2"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollRightEvent_Raised_UpdatesBothImageSources()
        {
            var panels = CreatePanels("image1", "image2");
            var testSystemWrapper = new TestSystemWrapper();
            var eventAggregator = EventAggregatorBuilder.Create().Build();
            var control = CreatePanelsControl().WithPanels(panels).WithSystemWrapper(testSystemWrapper).WithEventAggregator(eventAggregator).Build();

            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Right);

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image2"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollLeftEvent_RaisedAndFarLeftPanelSelected_WrapsAroundAndUpdatesBothImageSources()
        {
            var panels = CreatePanels("image1", "image2", "image3");
            var testSystemWrapper = new TestSystemWrapper();
            var eventAggregator = EventAggregatorBuilder.Create().Build();
            var control = CreatePanelsControl().WithPanels(panels).WithSystemWrapper(testSystemWrapper).WithEventAggregator(eventAggregator).Build();

            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Left);

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image3"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollRightEvents_RaisedBeforeFirstScrollCompletes_IgnoresScrollEventsWhileStillScrolling()
        {
            var panels = CreatePanels("image1", "image2", "image3");
            var testSystemWrapper = new TestSystemWrapper();
            var eventAggregator = EventAggregatorBuilder.Create().Build();
            var control = CreatePanelsControl().WithPanels(panels).WithSystemWrapper(testSystemWrapper).WithEventAggregator(eventAggregator).Build();

            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Right);

            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Right);
            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Right);
            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Right);

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image2"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image1"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollLeftEvents_RaisedAfterEachScrollCompletes_ScrollsToExpectedPanel()
        {
            var panels = CreatePanels("image1", "image2", "image3", "image4", "image5");
            var testSystemWrapper = new TestSystemWrapper();
            var eventAggregator = EventAggregatorBuilder.Create().Build();
            var control = CreatePanelsControl().WithPanels(panels).WithSystemWrapper(testSystemWrapper).WithEventAggregator(eventAggregator).Build();

            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Left);
            control.FinishScrollingCommand.Execute(this);
            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Left);
            control.FinishScrollingCommand.Execute(this);
            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Left);
            control.FinishScrollingCommand.Execute(this);

            Assert.Equal<ImageSource>(control.OnScreenImageSource, testSystemWrapper.Images["image3"] as ImageSource);
            Assert.Equal<ImageSource>(control.OffScreenImageSource, testSystemWrapper.Images["image4"] as ImageSource);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void FinishScrollingCommand_InvokedAfterAScroll_RaisesFinishScrolllingEvent()
        {
            var finishScrollingEventRaised = false;
            var panels = CreatePanels("image1", "image2");
            var testSystemWrapper = new TestSystemWrapper();
            var eventAggregator = EventAggregatorBuilder.Create().WithFinishScrollingEventHandler((d) => { finishScrollingEventRaised = true; }).Build();
            var control = CreatePanelsControl().WithPanels(panels).WithSystemWrapper(testSystemWrapper).WithEventAggregator(eventAggregator).Build();

            eventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Right);
            control.FinishScrollingCommand.Execute(this);

            Assert.True(finishScrollingEventRaised);
        }

        private PanelsControlViewModelBuilder CreatePanelsControl()
        {
            return new PanelsControlViewModelBuilder();
        }

        private List<PanelsPanel> CreatePanels(params string[] filenames)
        {
            var panels = new List<PanelsPanel>();

            foreach (var filename in filenames)
            {
                panels.Add(new PanelsPanel() { Background = filename });
            }
            return panels;
        }

        private class TestSystemWrapper : SystemWrapperBase
        {
            public Dictionary<string, BitmapImage> Images = new Dictionary<string, BitmapImage>();

            public override BitmapImage CreateBitmapImage(string filename)
            {
                Images[filename] = new BitmapImage();
                return Images[filename];
            }
        }
    }
}
