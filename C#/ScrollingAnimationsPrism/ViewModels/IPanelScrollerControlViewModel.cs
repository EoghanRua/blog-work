﻿using System.Windows.Input;

namespace ScrollingAnimationsPrism.ViewModels
{
    interface IPanelScrollerControlViewModel
    {
        ICommand ScrollLeftCommand { get; }
        ICommand ScrollRightCommand { get; }
        bool ScrollLeft { get; }
        bool ScrollRight { get; }
        bool ScrollLeftWrap { get; }
        bool ScrollRightWrap { get; }
        uint NumPanels { get; set; }

        void FinishScrollingHandler(object data);
    }
}
