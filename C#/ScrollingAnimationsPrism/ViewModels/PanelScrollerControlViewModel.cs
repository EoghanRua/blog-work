﻿using System;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using ScrollingAnimationsPrism.Utiltilities;

namespace ScrollingAnimationsPrism.ViewModels
{
    public sealed class PanelScrollerControlViewModel : BindableBase, IPanelScrollerControlViewModel
    {
        private readonly ICommand mScrollLeftCommand;
        public ICommand ScrollLeftCommand
        {
            get { return mScrollLeftCommand; }
        }

        private readonly ICommand mScrollRightCommand;
        public ICommand ScrollRightCommand
        {
            get { return mScrollRightCommand; }
        }

        private bool mScrollLeft = false;
        public bool ScrollLeft
        {
            get { return mScrollLeft; }
            internal set { SetProperty(ref mScrollLeft, value); }
        }

        private bool mScrollRight = false;
        public bool ScrollRight
        {
            get { return mScrollRight; }
            internal set { SetProperty(ref mScrollRight, value); }
        }

        private bool mScrollLeftWrap = false;
        public bool ScrollLeftWrap
        {
            get { return mScrollLeftWrap; }
            internal set { SetProperty(ref mScrollLeftWrap, value); }
        }

        private bool mScrollRightWrap = false;
        public bool ScrollRightWrap
        {
            get { return mScrollRightWrap; }
            internal set { SetProperty(ref mScrollRightWrap, value); }
        }        

        private uint mNumPanels;
        public uint NumPanels
        {
            get { return mNumPanels; }
            set
            {
                if (value == 0)
                    throw new ArgumentException("NumPanels must be at least 1", "NumPanels");

                SetProperty(ref mNumPanels, value);
                mMaxIndex = value - 1;
            }
        }
        
        private bool mIsScrolling = false;
        private uint mCurrentIndex = 0;
        private uint mMaxIndex = 0;
        private readonly IEventAggregator mEventAggregator;

        public PanelScrollerControlViewModel(IEventAggregator eventAggregator)
        {
            if (eventAggregator == null)
                throw new ArgumentNullException("eventAggregator");

            mEventAggregator = eventAggregator;

            mScrollLeftCommand = new DelegateCommand(() =>
            {
                if (!mIsScrolling)
                {
                    if (mCurrentIndex == 0)
                    {
                        mCurrentIndex = mMaxIndex;
                        ScrollLeftWrap = true;
                    }
                    else
                    {
                        --mCurrentIndex;
                        ScrollLeft = true;
                    }

                    mIsScrolling = true;
                    mEventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Left);
                }
            });

            mScrollRightCommand = new DelegateCommand(() =>
            {
                if (!mIsScrolling)
                {
                    if (mCurrentIndex == mMaxIndex)
                    {
                        mCurrentIndex = 0;
                        ScrollRightWrap = true;
                    }
                    else
                    {
                        ++mCurrentIndex;
                        ScrollRight = true;
                    }

                    mIsScrolling = true;
                    mEventAggregator.GetEvent<ScrollEvent>().Publish(Scroll.Direction.Right);
                }
            });

            mEventAggregator.GetEvent<FinishScrollingEvent>().Subscribe(FinishScrollingHandler);
            mEventAggregator.GetEvent<PanelsLoadedEvent>().Subscribe((n) => {NumPanels = n;});
        }

        public void FinishScrollingHandler(object data)
        {
            if (mIsScrolling)
            {
                mIsScrolling = false;
                ScrollLeft = false;
                ScrollLeftWrap = false;
                ScrollRight = false;
                ScrollRightWrap = false;
            }
        }
    }
}
