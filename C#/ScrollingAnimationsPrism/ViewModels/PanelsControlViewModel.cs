﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using ScrollingAnimationsPrism.Services;
using ScrollingAnimationsPrism.Utiltilities;

namespace ScrollingAnimationsPrism.ViewModels
{
    public sealed class PanelsControlViewModel : BindableBase, IPanelsControlViewModel
    {
        private ImageSource mOnScreenImageSource;
        public ImageSource OnScreenImageSource
        {
            get { return mOnScreenImageSource; }
            set { SetProperty(ref mOnScreenImageSource, value); }
        }

        private ImageSource mOffScreenImageSource;
        public ImageSource OffScreenImageSource
        {
            get { return mOffScreenImageSource; }
            set { SetProperty(ref mOffScreenImageSource, value); }
        }

        private readonly ICommand mFinishScrollingCommand;
        public ICommand FinishScrollingCommand
        {
            get { return mFinishScrollingCommand; }
        }

        private bool mScrollLeft = false;
        public bool ScrollLeft
        {
            get { return (mScrollLeft); }
            set { SetProperty(ref mScrollLeft, value); }
        }

        private bool mScrollRight = false;
        public bool ScrollRight
        {
            get { return (mScrollRight); }
            set { SetProperty(ref mScrollRight, value); }
        }


        private readonly SystemWrapperBase mSystemWrapper;
        private readonly IEventAggregator mEventAggregator;
        private IPanelLoaderService mPanelLoaderService;
        private IList<ImageSource> mPanelImages = new List<ImageSource>();
        private int mCurrentPanel = 0;
        private bool mIsScrolling = false;

        public PanelsControlViewModel(IPanelLoaderService panelLoaderService, IEventAggregator eventAggregator)
            : this(SystemWrapper.Instance, panelLoaderService, eventAggregator) { }

        internal PanelsControlViewModel(SystemWrapperBase systemWrapper, IPanelLoaderService panelLoaderService, IEventAggregator eventAggregator)
        {
            mSystemWrapper = systemWrapper;

            if (panelLoaderService == null)
                throw new ArgumentNullException("panelLoaderService", "Needs to be set before attempting to hook up panels");

            mPanelLoaderService = panelLoaderService;

            if (eventAggregator == null)
                throw new ArgumentNullException("eventAggregator", "Required for communication between View Models");

            mEventAggregator = eventAggregator;

            mFinishScrollingCommand = new DelegateCommand(() =>
            {
                mIsScrolling = false;
                ScrollLeft = false;
                ScrollRight = false;

                mEventAggregator.GetEvent<FinishScrollingEvent>().Publish(null);
            });

            mEventAggregator.GetEvent<ScrollEvent>().Subscribe(ScrollHandler);
        }

        public async void HookupImagesToPanels()
        {   
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                return;
         
            var imageFilenames = await mPanelLoaderService.LoadPanels();
            foreach (var filename in imageFilenames)
            {
                var bi = mSystemWrapper.CreateBitmapImage(filename.Background);
                mPanelImages.Add(bi);
            }

            mEventAggregator.GetEvent<PanelsLoadedEvent>().Publish((uint)mPanelImages.Count);

            OnScreenImageSource = mPanelImages[mCurrentPanel];
            OffScreenImageSource = mPanelImages[mCurrentPanel + 1];
        }

        public void ScrollHandler(Scroll.Direction direction)
        {
            if (!mIsScrolling)
            {
                if (direction == Scroll.Direction.Left)
                {
                    ScrollPanelLeft();
                }
                else
                {
                    ScrollPanelRight();
                }

                mIsScrolling = true;
            }
        }

        private void ScrollPanelLeft()
        {
            if (mCurrentPanel == 0)
            {
                mCurrentPanel = mPanelImages.Count - 1;
            }
            else
            {                
                --mCurrentPanel;
            }

            UpdateImageSources();

            ScrollLeft = true;
        }

        private void ScrollPanelRight()
        {
            if (mCurrentPanel == (mPanelImages.Count - 1))
            {
                mCurrentPanel = 0;
            }
            else
            {
                ++mCurrentPanel;
            }

            UpdateImageSources();

            ScrollRight = true;
        }

        private void UpdateImageSources()
        {
            OffScreenImageSource = OnScreenImageSource;
            OnScreenImageSource = mPanelImages[mCurrentPanel];
        }
    }
}
