﻿using ScrollingAnimations.Utiltilities;
using ScrollingAnimations.ViewModel;

namespace ScrollingAnimations.Tests
{
    public class PanelScrollerControlBuilder
    {
        private ScrollEventsPublisher mScrollEventsPublisher;
        private bool mSetAllScrollFlags = false;
        private float mButtonWidthHeight = 4.0f;
        private float mMargin = 1.0f;
        private uint mNumPanels = 1;

        public PanelScrollerControlBuilder WithScrollEventsPublisher(ScrollEventsPublisher publisher)
        {
            mScrollEventsPublisher = publisher;
            return this;
        }

        public PanelScrollerControlBuilder WithAllScrollFlagsSet()
        {
            mSetAllScrollFlags = true;
            return this;
        }

        public PanelScrollerControlBuilder WithButtonWidthHeight(float buttonWidthHeight)
        {
            mButtonWidthHeight = buttonWidthHeight;
            return this;
        }

        public PanelScrollerControlBuilder WithMargin(float margin)
        {
            mMargin = margin;
            return this;
        }

        public PanelScrollerControlBuilder WithNumPanels(uint numPanels)
        {
            mNumPanels = numPanels;
            return this;
        }

        public PanelScrollerControl Build()
        {
            return this;
        }

        public static implicit operator PanelScrollerControl(PanelScrollerControlBuilder builder)
        {
            var control = new PanelScrollerControl(builder.mScrollEventsPublisher, builder.mButtonWidthHeight, builder.mMargin, builder.mNumPanels);

            if (builder.mSetAllScrollFlags)
            {
                control.ScrollLeft = true;
                control.ScrollLeftWrap = true;
                control.ScrollRight = true;
                control.ScrollRightWrap = true;
            }

            return control;
        }
    }
}
