﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using ScrollingAnimations.Services;
using ScrollingAnimations.Utiltilities;

namespace ScrollingAnimations.ViewModel
{
    public sealed class PanelsControl : PropertyChangedNotifier
    {
        private Nullable<float> mScrollLeftWidth = 0.0f;
        public Nullable<float> ScrollLeftWidth
        {
            get { return mScrollLeftWidth; }
            set
            {
                if (value == mScrollLeftWidth)
                    return;
                mScrollLeftWidth = value;
                OnPropertyChanged();
            }
        }

        private Nullable<float> mScrollRightWidth = 0.0f;
        public Nullable<float> ScrollRightWidth
        {
            get { return mScrollRightWidth; }
            set
            {
                if (value == mScrollRightWidth)
                    return;
                mScrollRightWidth = value;
                OnPropertyChanged();
            }
        }

        private ImageSource mOnScreenImageSource;
        public ImageSource OnScreenImageSource
        {
            get { return mOnScreenImageSource; }
            set 
            {
                if (value == mOnScreenImageSource)
                    return;
                mOnScreenImageSource = value;
                OnPropertyChanged();
            }
        }

        private ImageSource mOffScreenImageSource;
        public ImageSource OffScreenImageSource
        {
            get { return mOffScreenImageSource; }
            set
            {
                if (value == mOffScreenImageSource)
                    return;
                mOffScreenImageSource = value;
                OnPropertyChanged();
            }
        }

        private readonly ICommand mFinishScrollingCommand;
        public ICommand FinishScrollingCommand
        {
            get { return mFinishScrollingCommand; }
        }

        private bool mScrollLeft = false;
        public bool ScrollLeft
        {
            get { return (mScrollLeft); }
            set
            {
                if (value == mScrollLeft)
                    return;
                mScrollLeft = value;
                OnPropertyChanged();
            }
        }

        private bool mScrollRight = false;
        public bool ScrollRight
        {
            get { return (mScrollRight); }
            set
            {
                if (value == mScrollRight)
                    return;
                mScrollRight = value;
                OnPropertyChanged();
            }
        }

        private IPanelLoaderService mPanelLoaderService;
        
        private readonly SystemWrapperBase mSystemWrapper;
        private IList<ImageSource> mPanelImages = new List<ImageSource>();
        private int mCurrentPanel = 0;
        private bool mIsScrolling = false;

        public PanelsControl(float width)
            : this(SystemWrapper.Instance, ScrollEventsPublisher.Instance, new PanelLoaderService(), width) { }

        internal PanelsControl(SystemWrapperBase systemWrapper, ScrollEventsPublisher publisher, IPanelLoaderService panelLoaderService, float width)
        {
            mSystemWrapper = systemWrapper;

            if (panelLoaderService == null)
                throw new ArgumentNullException("panelLoaderService", "Needs to be set before attempting to hook up panels");

            mPanelLoaderService = panelLoaderService;

            if (publisher == null)
                throw new ArgumentNullException("publisher");

            publisher.ScrollEvent += ScrollHandler;

            mFinishScrollingCommand = new DelegateCommand(() =>
            {
                mIsScrolling = false;
                ScrollLeft = false;
                ScrollRight = false;

                publisher.Raise(new EventArgs());
            });


            ScrollLeftWidth = -width;
            ScrollRightWidth = width;
        }

        public async void HookupImagesToPanels()
        {   
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                return;
         
            var imageFilenames = await mPanelLoaderService.LoadPanels();
            foreach (var filename in imageFilenames)
            {
                var bi = mSystemWrapper.CreateBitmapImage(filename.Background);
                mPanelImages.Add(bi);
            }

            OnScreenImageSource = mPanelImages[mCurrentPanel];
            OffScreenImageSource = mPanelImages[mCurrentPanel + 1];
        }

        public void ScrollHandler(Object sender, ScrollEventArgs e)
        {
            if (!mIsScrolling)
            {
                if (e.ScrollDirection == ScrollEventArgs.Direction.Left)
                {
                    ScrollPanelLeft();
                }
                else
                {
                    ScrollPanelRight();
                }

                mIsScrolling = true;
            }
        }

        private void ScrollPanelLeft()
        {
            if (mCurrentPanel == 0)
            {
                mCurrentPanel = mPanelImages.Count - 1;
            }
            else
            {                
                --mCurrentPanel;
            }

            UpdateImageSources();

            ScrollLeft = true;
        }

        private void ScrollPanelRight()
        {
            if (mCurrentPanel == (mPanelImages.Count - 1))
            {
                mCurrentPanel = 0;
            }
            else
            {
                ++mCurrentPanel;
            }

            UpdateImageSources();

            ScrollRight = true;
        }

        private void UpdateImageSources()
        {
            OffScreenImageSource = OnScreenImageSource;
            OnScreenImageSource = mPanelImages[mCurrentPanel];
        }
    }
}
