﻿using System;
using System.Windows;
using System.Windows.Input;
using ScrollingAnimations.Utiltilities;

namespace ScrollingAnimations.ViewModel
{
    public sealed class PanelScrollerControl : PropertyChangedNotifier
    {
        private readonly ICommand mScrollLeftCommand;
        public ICommand ScrollLeftCommand
        {
            get { return mScrollLeftCommand; }
        }

        private readonly ICommand mScrollRightCommand;
        public ICommand ScrollRightCommand
        {
            get { return mScrollRightCommand; }
        }

        private bool mScrollLeft = false;
        public bool ScrollLeft
        {
            get { return (mScrollLeft); }
            set
            {
                if (value == mScrollLeft)
                    return;
                mScrollLeft = value;
                OnPropertyChanged();
            }
        }

        private bool mScrollRight = false;
        public bool ScrollRight
        {
            get { return (mScrollRight); }
            set
            {
                if (value == mScrollRight)
                    return;
                mScrollRight = value;
                OnPropertyChanged();
            }
        }

        private bool mScrollLeftWrap = false;
        public bool ScrollLeftWrap
        {
            get { return (mScrollLeftWrap); }
            set
            {
                if (value == mScrollLeftWrap)
                    return;
                mScrollLeftWrap = value;
                OnPropertyChanged();
            }
        }

        private bool mScrollRightWrap = false;
        public bool ScrollRightWrap
        {
            get { return (mScrollRightWrap); }
            set
            {
                if (value == mScrollRightWrap)
                    return;
                mScrollRightWrap = value;
                OnPropertyChanged();
            }
        }

        private Nullable<float> mScrollLeftWidth = 0.0f;
        public Nullable<float> ScrollLeftWidth
        {
            get { return mScrollLeftWidth; }
            set
            {
                if (value == mScrollLeftWidth)
                    return;
                mScrollLeftWidth = value;
                OnPropertyChanged();
            }
        }

        private Nullable<float> mScrollRightWidth = 0.0f;
        public Nullable<float> ScrollRightWidth
        {
            get { return mScrollRightWidth; }
            set
            {
                if (value == mScrollRightWidth)
                    return;
                mScrollRightWidth = value;
                OnPropertyChanged();
            }
        }

        private Nullable<float> mScrollToLeftEndWidth = 0.0f;
        public Nullable<float> ScrollToLeftEndWidth
        {
            get { return mScrollToLeftEndWidth; }
            set
            {
                if (value == mScrollToLeftEndWidth)
                    return;
                mScrollToLeftEndWidth = value;
                OnPropertyChanged();
            }
        }

        private Nullable<float> mScrollToRightEndWidth = 0.0f;
        public Nullable<float> ScrollToRightEndWidth
        {
            get { return mScrollToRightEndWidth; }
            set
            {
                if (value == mScrollToRightEndWidth)
                    return;
                mScrollToRightEndWidth = value;
                OnPropertyChanged();
            }
        }

        private float mHighlightedButtonWidthHeight = 0.0f;
        public float HighlightedButtonWidthHeight
        {
            get { return mHighlightedButtonWidthHeight; }
            set
            {
                if (value == mHighlightedButtonWidthHeight)
                    return;
                mHighlightedButtonWidthHeight = value;
                OnPropertyChanged();
            }
        }

        private float mTotalWidth = 0.0f;
        public float TotalWidth
        {
            get { return mTotalWidth; }
            set
            {
                if (value == mTotalWidth)
                    return;
                mTotalWidth = value;
                OnPropertyChanged();
            }
        }

        private Rect mViewportRect;
        public Rect ViewportRect
        {
            get { return mViewportRect; }
            set
            {
                if (value == mViewboxRect)
                    return;
                mViewportRect = value;
                OnPropertyChanged();
            }
        }
        
        private Rect mViewboxRect;
        public Rect ViewboxRect
        {
            get { return mViewboxRect; }
            set
            {
                if (value == mViewboxRect)
                    return;
                mViewboxRect = value;
                OnPropertyChanged();
            }
        }

        private Thickness mButtonMargin;
        public Thickness ButtonMargin
        {
            get { return mButtonMargin; }
            set
            {
                if (value == mButtonMargin)
                    return;
                mButtonMargin = value;
                OnPropertyChanged();
            }
        }

        private float mButtonWidthHeight;
        public float ButtonWidthHeight
        {
            get { return mButtonWidthHeight; }
            set
            {
                if (value == mButtonWidthHeight)
                    return;
                mButtonWidthHeight = value;
                OnPropertyChanged();
            }
        }
        
        private bool mIsScrolling = false;
        private uint mCurrentIndex = 0;
        private uint mMaxIndex = 0;

        public PanelScrollerControl(float buttonWidthHeight, float margin, uint numPanels)
            : this(ScrollEventsPublisher.Instance, buttonWidthHeight, margin, numPanels){}

        internal PanelScrollerControl(ScrollEventsPublisher publisher, float buttonWidthHeight, float margin, uint numPanels)
        {
            if(publisher == null)
                throw new ArgumentNullException("publisher");

            if (numPanels == 0)
                throw new ArgumentException("numPanels must be at least 1", "numPanels");

            if (margin >= (buttonWidthHeight / 2.0f))
                throw new ArgumentException("The margin must be less than half of the buttonWdithHeight", "margin");

            mScrollLeftCommand = new DelegateCommand(() =>
            {
                if (!mIsScrolling)
                {
                    if (mCurrentIndex == 0)
                    {
                        mCurrentIndex = mMaxIndex;
                        ScrollLeftWrap = true;
                    }
                    else
                    {
                        --mCurrentIndex;
                        ScrollLeft = true;
                    }

                    mIsScrolling = true;
                    publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Left));
                }
            });

            mScrollRightCommand = new DelegateCommand(() =>
            {
                if (!mIsScrolling)
                {
                    if (mCurrentIndex == mMaxIndex)
                    {
                        mCurrentIndex = 0;
                        ScrollRightWrap = true;
                    }
                    else
                    {
                        ++mCurrentIndex;
                        ScrollRight = true;
                    }

                    mIsScrolling = true;
                    publisher.Raise(new ScrollEventArgs(ScrollEventArgs.Direction.Right));
                }
            });

            publisher.FinishScrollingEvent += FinishScrollingHandler;

            ButtonWidthHeight = buttonWidthHeight;
            ScrollLeftWidth = -ButtonWidthHeight;
            ScrollRightWidth = ButtonWidthHeight;
            ScrollToLeftEndWidth = -((numPanels - 1) * ButtonWidthHeight);
            ScrollToRightEndWidth = (numPanels - 1) * ButtonWidthHeight;
            TotalWidth = (numPanels + 2) * ButtonWidthHeight; // 2 to account for the arrows
            ViewportRect = new Rect(margin, margin, ButtonWidthHeight, ButtonWidthHeight);
            var imageSizeComparedToButtonSize = 1.0 - ((margin * 2.0) / ButtonWidthHeight);  // 2.0 to account for margin on both sides (i.e. top & bottomw, left & right)
            var zoomFactor = 1.0 / imageSizeComparedToButtonSize;
            ViewboxRect = new Rect(0.0, 0.0, zoomFactor, zoomFactor);
            HighlightedButtonWidthHeight = ButtonWidthHeight - (margin * 2.0f); // 2.0f to account for margin on both sides (i.e. top & bottomw, left & right)
            ButtonMargin = new Thickness(margin, margin, (TotalWidth - (3.0f * ButtonWidthHeight) + margin), margin); // 3.0f for both arrow buttons and a single radio button

            mMaxIndex = numPanels - 1;
        }

        public void FinishScrollingHandler(Object sender, EventArgs e)
        {
            if (mIsScrolling)
            {
                mIsScrolling = false;
                ScrollLeft = false;
                ScrollLeftWrap = false;
                ScrollRight = false;
                ScrollRightWrap = false;
            }
        }
    }
}
