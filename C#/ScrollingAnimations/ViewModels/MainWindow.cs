﻿
namespace ScrollingAnimations.ViewModel
{
    public sealed class MainWindow : PropertyChangedNotifier
    {
        private PanelsControl mPanels;
        public PanelsControl Panels
        {
            get { return mPanels; }
            set
            {
                if (value == mPanels)
                    return;
                mPanels = value;
                OnPropertyChanged();
            }
        }

        private PanelScrollerControl mPanelScroller;
        public PanelScrollerControl PanelScroller
        {
            get { return mPanelScroller; }
            set
            {
                if (value == mPanelScroller)
                    return;
                mPanelScroller = value;
                OnPropertyChanged();
            }
        }
        

        public MainWindow()
        {
            Panels = new PanelsControl(435.0f);
            PanelScroller = new PanelScrollerControl(50.0f, 7.5f, 4);
        }
    }
}
