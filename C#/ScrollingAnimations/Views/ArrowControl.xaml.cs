﻿using System.Windows;
using System.Windows.Controls;

namespace ScrollingAnimations.View
{
    /// <summary>
    /// Interaction logic for ArrowControl.xaml
    /// </summary>
    public partial class ArrowControl : UserControl
    {
        public enum ArrowDirection
        {
            Left,
            Right
        };

        public static readonly DependencyProperty DirectionProperty = DependencyProperty.Register("Direction", typeof(ArrowDirection), typeof(ArrowControl), new PropertyMetadata(ArrowDirection.Left));
        public ArrowDirection Direction
        {
            get { return (ArrowDirection)GetValue(DirectionProperty); }
            set { SetValue(DirectionProperty, value); }
        }

        public ArrowControl()
        {
            InitializeComponent();
        }
    }
}
