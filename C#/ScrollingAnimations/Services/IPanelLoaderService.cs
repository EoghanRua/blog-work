﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScrollingAnimations.Services
{
    public interface IPanelLoaderService
    {
        Task<IEnumerable<PanelsPanel>> LoadPanels();
    }
}
