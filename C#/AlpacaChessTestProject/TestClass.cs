﻿using System;
using System.Threading.Tasks;
using Microsoft.Concurrency.TestTools.UnitTesting;
using Microsoft.Concurrency.TestTools.UnitTesting.Chess;

namespace AlpacaChessTestProject
{
    public class TestClass
    {
        [UnitTestMethod]
        public void TestMethod()
        {
            Console.WriteLine("Hello World");
            Console.Error.WriteLine("This is an ERROR");
        }

        [UnitTestMethod]
        [TestArgs(100)]
        [TestArgs(1000)]
        public void SortArrayTest(int n)
        {
            var rand = new Random();

            // Create array of random integers
            int[] a = new int[n];
            for(var i = 0; i < n; ++i)
                a[i] = rand.Next();

            // Sort the array
            SortArray(a);

            // Verify Correctness
            for(var i = 1; i < n; ++i)
                Assert.IsTrue(a[i] >= a[i-1] , "Element {0} is not sorted correctly", i);
        }

        TaskMeter initMeter = new TaskMeter("InitializeData");
        TaskMeter sortingMeter = new TaskMeter("Sort");
        TaskMeter verificationMeter = new TaskMeter("Verify");

        [PerformanceTestMethod(Repetitions=5, WarmupRepetitions=2)]
        public void SortArrayPerfTest()
        {
            var n = 1000;
            var rand = new Random();

            // Create array of random integers
            initMeter.Start();
            int[] a = new int[n];
            for (int i = 0; i < n; ++i)
                a[i] = rand.Next();
            initMeter.End();

            // Sort the array
            sortingMeter.Start();
            SortArray(a);
            sortingMeter.End();

            // Verify Correctness
            verificationMeter.Start();
            try
            {
                for(int i = 1; i < n; ++i)
                    Assert.IsTrue(a[i] >= a[i - 1] , "Element {0} is not sorted correctly", i);
            }
            finally
            {
                verificationMeter.End();
            }
        }

        [ScheduleTestMethod]
        public void SimpleDeadlock()
        {
            object syncObj1 = new object();
            object syncObj2 = new object();

            Parallel.Invoke(
                () =>
                {
                    lock (syncObj1)
                        lock (syncObj2)
                        {}
                },
                () =>
                {
                    // Deadlock
                    //lock (syncObj2)
                    //    lock (syncObj1)
                    //    {}
                    lock (syncObj1)
                        lock (syncObj2)
                        { }
                });
        }

        [DataRaceTestMethod]
        public void SimpleDataRace()
        {
            int cnt = 0;
            Parallel.Invoke(
                () => cnt++,
                () => cnt++);

            Assert.AreEqual(2, cnt);
        }

        [ChessTestMethod()]
        [ChessTestContext(
            PreemptAllAccesses = true
            , ExtraCommandLineArgs = new[]{
                "/dpt:AntisocialRobots.RobotSimulationBase"
                , "/dpt:AlpacaProject.RobotSimulationFixes"
            })]
        public void NaiveParallel_ScheduleTest()
        { }

        private void SortArray(int[] a)
        {
            Array.Sort(a);
        }            
    }
}
