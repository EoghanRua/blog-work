﻿using System.Windows;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using ScrollingAnimationsMVVMLight.Services;
using ScrollingAnimationsMVVMLight.ViewModels;

namespace ScrollingAnimationsMVVMLight
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            SimpleIoc.Default.Register<IMainWindow, MainWindow>();
            SimpleIoc.Default.Register<IMessenger, Messenger>();
            SimpleIoc.Default.Register<IPanelLoaderService, PanelLoaderService>();
            SimpleIoc.Default.Register<IPanelsControl, PanelsControl>();
            SimpleIoc.Default.Register<IPanelScrollerControl, PanelScrollerControl>();

            var window = new Views.MainWindow();
            window.Show();
        }
    }
}
