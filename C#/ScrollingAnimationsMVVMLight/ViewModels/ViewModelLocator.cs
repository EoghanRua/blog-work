﻿using System;
using System.Globalization;
using System.Reflection;
using System.Windows;
using GalaSoft.MvvmLight.Ioc;

namespace ScrollingAnimationsMVVMLight.ViewModels
{
    public static class ViewModelLocationProvider
    {
        /// <summary>
        /// The ViewModel factory which defers to the SimpleIoc for creating instances of the ViewModel
        /// </summary>
        static Func<Type, object> mViewModelFactory = type => SimpleIoc.Default.GetInstance(type);
        
        /// <summary>
        /// Default view type to view model type resolver, assumes the view model is in same assembly as the view type, but in the "ViewModels" namespace.
        /// </summary>
        static Func<Type, Type> mViewTypeToViewModelTypeResolver = viewType => {
                var viewName = viewType.FullName;
                viewName = viewName.Replace(".Views.", ".ViewModels.I");
                var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
                var viewModelInterfaceName = String.Format(CultureInfo.InvariantCulture, "{0}, {1}", viewName, viewAssemblyName);
                return Type.GetType(viewModelInterfaceName);
            };

        /// <summary>
        /// Automatically looks up the vVewModel that corresponds to the current view using a naming convention strategy:
        /// Swap the Views namespace for ViewModels in the full type name
        /// </summary>
        /// <param name="view">The dependency object, typically a view</param>
        /// <param name="setDataContextCallback">The call back to use to create the binding between the View and ViewModel</param>
        public static void AutoWireViewModelChanged(object view, Action<object, object> setDataContextCallback)
        {
            var viewModelType = mViewTypeToViewModelTypeResolver(view.GetType());
            if (viewModelType == null)
                return;

            var viewModel = mViewModelFactory(viewModelType);
            
            setDataContextCallback(view, viewModel);
        }
    }

    /// <summary>
    /// Class for locating ViewModels for Views via an attached property
    /// </summary>
    public static class ViewModelLocator
    {
        /// <summary>
        /// The AutoWireViewModel attached property.
        /// </summary>
        public static DependencyProperty AutoWireViewModelProperty = DependencyProperty.RegisterAttached(
            "AutoWireViewModel",
            typeof(bool),
            typeof(ViewModelLocator),
            new PropertyMetadata(false, AutoWireViewModelChanged));

        public static bool GetAutoWireViewModel(DependencyObject obj)
        {
            return (bool)obj.GetValue(AutoWireViewModelProperty);
        }
        public static void SetAutoWireViewModel(DependencyObject obj, bool value)
        {
            obj.SetValue(AutoWireViewModelProperty, value);
        }

        private static void AutoWireViewModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
                ViewModelLocationProvider.AutoWireViewModelChanged(d, Bind);
        }

        /// <summary>
        /// Sets the DataContext of a View
        /// </summary>
        /// <param name="view">The View to set the DataContext on</param>
        /// <param name="dataContext">The object to use as the DataContext for the View</param>
        static void Bind(object view, object viewModel)
        {
            FrameworkElement element = view as FrameworkElement;
            if (element != null)
                element.DataContext = viewModel;
        }
    }
}
