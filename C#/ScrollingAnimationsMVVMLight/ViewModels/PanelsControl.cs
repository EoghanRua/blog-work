﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using ScrollingAnimationsMVVMLight.Services;
using ScrollingAnimationsMVVMLight.Utiltilities;

namespace ScrollingAnimationsMVVMLight.ViewModels
{
    public sealed class PanelsControl : ViewModelBase, IPanelsControl
    {
        private ImageSource mOnScreenImageSource;
        public ImageSource OnScreenImageSource
        {
            get { return mOnScreenImageSource; }
            set { Set(ref mOnScreenImageSource, value); }
        }

        private ImageSource mOffScreenImageSource;
        public ImageSource OffScreenImageSource
        {
            get { return mOffScreenImageSource; }
            set { Set(ref mOffScreenImageSource, value); }
        }

        private readonly ICommand mFinishScrollingCommand;
        public ICommand FinishScrollingCommand
        {
            get { return mFinishScrollingCommand; }
        }

        private bool mScrollLeft = false;
        public bool ScrollLeft
        {
            get { return (mScrollLeft); }
            set { Set(ref mScrollLeft, value); }
        }

        private bool mScrollRight = false;
        public bool ScrollRight
        {
            get { return (mScrollRight); }
            set { Set(ref mScrollRight, value); }
        }
        
        private readonly SystemWrapperBase mSystemWrapper;
        private readonly IPanelLoaderService mPanelLoaderService;
        private IList<ImageSource> mPanelImages = new List<ImageSource>();
        private readonly IMessenger mMessenger;
        private int mCurrentPanel = 0;
        private bool mIsScrolling = false;

        [PreferredConstructor]
        public PanelsControl(IMessenger messenger, IPanelLoaderService panelLoaderService)
            : this(SystemWrapper.Instance, messenger, panelLoaderService) { }

        internal PanelsControl(SystemWrapperBase systemWrapper, IMessenger messenger, IPanelLoaderService panelLoaderService)
        {
            mSystemWrapper = systemWrapper;

            if (messenger == null)
                throw new ArgumentNullException("messenger");

            mMessenger = messenger;
            mMessenger.Register<ScrollEvent>(this, ScrollHandler);

            if (panelLoaderService == null)
                throw new ArgumentNullException("panelLoaderService", "Needs to be set before attempting to hook up panels");

            mPanelLoaderService = panelLoaderService;

            mFinishScrollingCommand = new RelayCommand(() =>
            {
                mIsScrolling = false;
                ScrollLeft = false;
                ScrollRight = false;

                mMessenger.Send(new FinishScrollingEvent());
            });
        }

        public async void HookupImagesToPanels()
        {   
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
                return;

            var imageFilenames = await mPanelLoaderService.LoadPanels();
            foreach (var filename in imageFilenames)
            {
                var bi = mSystemWrapper.CreateBitmapImage(filename.Background);
                mPanelImages.Add(bi);
            }

            mMessenger.Send(new PanelsLoadedEvent((uint)mPanelImages.Count));

            OnScreenImageSource = mPanelImages[mCurrentPanel];
            OffScreenImageSource = mPanelImages[mCurrentPanel + 1];
        }

        public void ScrollHandler(ScrollEvent e)
        {
            if (!mIsScrolling)
            {
                if (e.Content == Scroll.Direction.Left)
                {
                    ScrollPanelLeft();
                }
                else
                {
                    ScrollPanelRight();
                }

                mIsScrolling = true;
            }
        }

        private void ScrollPanelLeft()
        {
            if (mCurrentPanel == 0)
            {
                mCurrentPanel = mPanelImages.Count - 1;
            }
            else
            {                
                --mCurrentPanel;
            }

            UpdateImageSources();

            ScrollLeft = true;
        }

        private void ScrollPanelRight()
        {
            if (mCurrentPanel == (mPanelImages.Count - 1))
            {
                mCurrentPanel = 0;
            }
            else
            {
                ++mCurrentPanel;
            }

            UpdateImageSources();

            ScrollRight = true;
        }

        private void UpdateImageSources()
        {
            OffScreenImageSource = OnScreenImageSource;
            OnScreenImageSource = mPanelImages[mCurrentPanel];
        }
    }
}
