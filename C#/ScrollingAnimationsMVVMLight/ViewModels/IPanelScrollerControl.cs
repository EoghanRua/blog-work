﻿using System.Windows.Input;
using ScrollingAnimationsMVVMLight.Utiltilities;

namespace ScrollingAnimationsMVVMLight.ViewModels
{
    interface IPanelScrollerControl
    {
        ICommand ScrollLeftCommand { get; }
        ICommand ScrollRightCommand { get; }
        bool ScrollLeft { get; }
        bool ScrollRight { get; }
        bool ScrollLeftWrap { get; }
        bool ScrollRightWrap { get; }
        uint NumPanels { get; set; }

        void FinishScrollingHandler(FinishScrollingEvent e);
    }
}
