﻿using System.Windows.Input;
using System.Windows.Media;
using ScrollingAnimationsMVVMLight.Utiltilities;

namespace ScrollingAnimationsMVVMLight.ViewModels
{
    interface IPanelsControl
    {
        ImageSource OnScreenImageSource { get; set; }
        ImageSource OffScreenImageSource { get; set; }
        ICommand FinishScrollingCommand { get; }
        bool ScrollLeft { get; set; }
        bool ScrollRight { get; set; }

        void HookupImagesToPanels();
        void ScrollHandler(ScrollEvent e);
    }
}
