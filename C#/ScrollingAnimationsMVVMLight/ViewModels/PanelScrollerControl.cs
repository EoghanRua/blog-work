﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using ScrollingAnimationsMVVMLight.Utiltilities;

namespace ScrollingAnimationsMVVMLight.ViewModels
{
    public sealed class PanelScrollerControl : ViewModelBase, IPanelScrollerControl
    {
        private ICommand mScrollLeftCommand;
        public ICommand ScrollLeftCommand
        {
            get
            {
                return mScrollLeftCommand ??
                    (mScrollLeftCommand = new RelayCommand(() =>
                    {
                        if (!mIsScrolling)
                        {
                            if (mCurrentIndex == 0)
                            {
                                mCurrentIndex = mMaxIndex;
                                ScrollLeftWrap = true;
                            }
                            else
                            {
                                --mCurrentIndex;
                                ScrollLeft = true;
                            }

                            mIsScrolling = true;
                            mMessenger.Send(new ScrollEvent(Scroll.Direction.Left));
                        }
                    })); 
            }
        }

        private ICommand mScrollRightCommand;
        public ICommand ScrollRightCommand
        {
            get
            {
                return mScrollRightCommand ??
                    (mScrollRightCommand = new RelayCommand(() =>
                    {
                        if (!mIsScrolling)
                        {
                            if (mCurrentIndex == mMaxIndex)
                            {
                                mCurrentIndex = 0;
                                ScrollRightWrap = true;
                            }
                            else
                            {
                                ++mCurrentIndex;
                                ScrollRight = true;
                            }

                            mIsScrolling = true;
                            mMessenger.Send(new ScrollEvent(Scroll.Direction.Right));
                        }
                    }));
            }
        }

        private bool mScrollLeft = false;
        public bool ScrollLeft
        {
            get { return (mScrollLeft); }
            set { Set(ref mScrollLeft, value); }
        }

        private bool mScrollRight = false;
        public bool ScrollRight
        {
            get { return (mScrollRight); }
            set { Set(ref mScrollRight, value); }
        }

        private bool mScrollLeftWrap = false;
        public bool ScrollLeftWrap
        {
            get { return (mScrollLeftWrap); }
            set { Set(ref mScrollLeftWrap, value); }
        }

        private bool mScrollRightWrap = false;
        public bool ScrollRightWrap
        {
            get { return (mScrollRightWrap); }
            set { Set(ref mScrollRightWrap, value); }
        }
        private uint mNumPanels;
        public uint NumPanels
        {
            get { return mNumPanels; }
            set
            {
                if (value == 0)
                    throw new ArgumentException("NumPanels must be at least 1", "NumPanels");

                Set(ref mNumPanels, value);
                mMaxIndex = value - 1;
            }
        }

        private readonly IMessenger mMessenger;
        private bool mIsScrolling = false;
        private uint mCurrentIndex = 0;
        private uint mMaxIndex = 0;

        public PanelScrollerControl(IMessenger messenger)
        {
            if (messenger == null)
                throw new ArgumentNullException("messenger");

            mMessenger = messenger;
            mMessenger.Register<FinishScrollingEvent>(this, FinishScrollingHandler);
            messenger.Register<PanelsLoadedEvent>(this, e => NumPanels = e.Content);
        }

        public void FinishScrollingHandler(FinishScrollingEvent e)
        {
            if (mIsScrolling)
            {
                mIsScrolling = false;
                ScrollLeft = false;
                ScrollLeftWrap = false;
                ScrollRight = false;
                ScrollRightWrap = false;
            }
        }
    }
}
