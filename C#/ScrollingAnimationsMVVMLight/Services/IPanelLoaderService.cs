﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScrollingAnimationsMVVMLight.Services
{
    public interface IPanelLoaderService
    {
        Task<IEnumerable<PanelsPanel>> LoadPanels();
    }
}
