﻿using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace ScrollingAnimationsMVVMLight.Utiltilities
{
    public class SystemWrapperBase
    {
        public virtual BitmapImage CreateBitmapImage(string filename) { return new BitmapImage(); }
    }

    public sealed class SystemWrapper : SystemWrapperBase
    {
        private static readonly SystemWrapper mInstance = new SystemWrapper();
        public static SystemWrapper Instance
        {
            get { return mInstance; }
        }

        public override BitmapImage CreateBitmapImage(string filename)
        {
            var path = Path.Combine(Environment.CurrentDirectory, filename);
            return new BitmapImage(new Uri(path));
        }
    }
}
