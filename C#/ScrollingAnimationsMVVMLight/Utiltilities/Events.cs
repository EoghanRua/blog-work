﻿using GalaSoft.MvvmLight.Messaging;

namespace ScrollingAnimationsMVVMLight.Utiltilities
{
    public class Scroll
    {
        public enum Direction { Left, Right };
    }

    public class ScrollEvent : GenericMessage<Scroll.Direction>
    {
        public ScrollEvent(Scroll.Direction direction) : base(direction) { }
    }

    public class FinishScrollingEvent : MessageBase { }

    public class PanelsLoadedEvent : GenericMessage<uint>
    {
        public PanelsLoadedEvent(uint numPanels) : base(numPanels) { }
    }
}
