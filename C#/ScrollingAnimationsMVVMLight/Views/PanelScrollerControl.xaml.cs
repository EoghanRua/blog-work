﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ScrollingAnimationsMVVMLight.Views
{
    /// <summary>
    /// Interaction logic for PanelScrollerControl.xaml
    /// </summary>
    public partial class PanelScrollerControl : UserControl
    {
        public static readonly DependencyProperty NumButtonsProperty = DependencyProperty.Register("NumButtons", typeof(uint), typeof(PanelScrollerControl));
        public uint NumButtons
        {
            get { return (uint)GetValue(NumButtonsProperty); }
            set { SetValue(NumButtonsProperty, value); }
        }

        public static readonly DependencyProperty ButtonWidthHeightProperty = DependencyProperty.Register("ButtonWidthHeight", typeof(float), typeof(PanelScrollerControl));
        public float ButtonWidthHeight
        {
            get { return (float)GetValue(ButtonWidthHeightProperty); }
            set { SetValue(ButtonWidthHeightProperty, value); }
        }

        public static readonly DependencyProperty MarginValueProperty = DependencyProperty.Register("MarginValue", typeof(float), typeof(PanelScrollerControl));
        public float MarginValue
        {
            get { return (float)GetValue(MarginValueProperty); }
            set
            {
                if (value >= (ButtonWidthHeight / 2.0f))
                    throw new ArgumentException("The margin must be less than half of the buttonWdithHeight", "MarginValue");

                SetValue(MarginValueProperty, value);
            }
        }

        public static readonly DependencyProperty ScrollLeftWidthProperty = DependencyProperty.Register("ScrollLeftWidth", typeof(float), typeof(PanelScrollerControl));
        public float ScrollLeftWidth
        {
            get { return (float)GetValue(ScrollLeftWidthProperty); }
            private set { SetValue(ScrollLeftWidthProperty, value); }
        }

        public static readonly DependencyProperty ScrollRightWidthProperty = DependencyProperty.Register("ScrollRightWidth", typeof(float), typeof(PanelScrollerControl));
        public float ScrollRightWidth
        {
            get { return (float)GetValue(ScrollRightWidthProperty); }
            private set { SetValue(ScrollRightWidthProperty, value); }
        }

        public static readonly DependencyProperty ScrollToLeftEndWidthProperty = DependencyProperty.Register("ScrollToLeftEndWidth", typeof(float), typeof(PanelScrollerControl));
        public float ScrollToLeftEndWidth
        {
            get { return (float)GetValue(ScrollToLeftEndWidthProperty); }
            private set { SetValue(ScrollToLeftEndWidthProperty, value); }
        }

        public static readonly DependencyProperty ScrollToRightEndWidthProperty = DependencyProperty.Register("ScrollToRightEndWidth", typeof(float), typeof(PanelScrollerControl));
        public float ScrollToRightEndWidth
        {
            get { return (float)GetValue(ScrollToRightEndWidthProperty); }
            private set { SetValue(ScrollToRightEndWidthProperty, value); }
        }

        public static readonly DependencyProperty TotalWidthProperty = DependencyProperty.Register("TotalWidth", typeof(float), typeof(PanelScrollerControl));
        public float TotalWidth
        {
            get { return (float)GetValue(TotalWidthProperty); }
            private set { SetValue(TotalWidthProperty, value); }
        }

        public static readonly DependencyProperty HighlightedButtonWidthHeightProperty = DependencyProperty.Register("HighlightedButtonWidthHeight", typeof(float), typeof(PanelScrollerControl));
        public float HighlightedButtonWidthHeight
        {
            get { return (float)GetValue(HighlightedButtonWidthHeightProperty); }
            private set { SetValue(HighlightedButtonWidthHeightProperty, value); }
        }

        public static readonly DependencyProperty ViewportRectProperty = DependencyProperty.Register("ViewportRect", typeof(Rect), typeof(PanelScrollerControl));
        public Rect ViewportRect
        {
            get { return (Rect)GetValue(ViewportRectProperty); }
            private set { SetValue(ViewportRectProperty, value); }
        }

        public static readonly DependencyProperty ViewboxRectProperty = DependencyProperty.Register("ViewboxRect", typeof(Rect), typeof(PanelScrollerControl));
        public Rect ViewboxRect
        {
            get { return (Rect)GetValue(ViewboxRectProperty); }
            private set { SetValue(ViewboxRectProperty, value); }
        }

        public static readonly DependencyProperty ButtonMarginProperty = DependencyProperty.Register("ButtonMargin", typeof(Thickness), typeof(PanelScrollerControl));
        public Thickness ButtonMargin
        {
            get { return (Thickness)GetValue(ButtonMarginProperty); }
            private set { SetValue(ButtonMarginProperty, value); }
        }

        public PanelScrollerControl()
        {
            InitializeComponent();
        }

        public void UpdateDimensions()
        {
            TotalWidth = (NumButtons + 2) * ButtonWidthHeight; // +2 to account for the arrows
            ScrollLeftWidth = -ButtonWidthHeight;
            ScrollRightWidth = ButtonWidthHeight;
            var scrollToEndWidth = (NumButtons - 1) * ButtonWidthHeight;
            ScrollToLeftEndWidth = -scrollToEndWidth;
            ScrollToRightEndWidth = scrollToEndWidth;
            ViewportRect = new Rect(MarginValue, MarginValue, ButtonWidthHeight, ButtonWidthHeight);
            var imageSizeComparedToButtonSize = 1.0 - ((MarginValue * 2.0) / ButtonWidthHeight);  // 2.0 to account for MarginValue on both sides (i.e. top & bottomw, left & right)
            var zoomFactor = 1.0 / imageSizeComparedToButtonSize;
            ViewboxRect = new Rect(0.0, 0.0, zoomFactor, zoomFactor);
            HighlightedButtonWidthHeight = ButtonWidthHeight - (MarginValue * 2.0f); // 2.0f to account for MarginValue on both sides (i.e. top & bottomw, left & right)
            ButtonMargin = new Thickness(MarginValue, MarginValue, (TotalWidth - (3.0f * ButtonWidthHeight) + MarginValue), MarginValue); // 3.0f for both arrow buttons and a single radio button
        }
    }
}
