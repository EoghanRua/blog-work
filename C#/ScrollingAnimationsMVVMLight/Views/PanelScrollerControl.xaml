﻿<UserControl x:Class="ScrollingAnimationsMVVMLight.Views.PanelScrollerControl"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:v="clr-namespace:ScrollingAnimationsMVVMLight.Views"
             xmlns:vm="clr-namespace:ScrollingAnimationsMVVMLight.ViewModels"
             xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"    
             xmlns:ic="clr-namespace:Microsoft.Expression.Interactivity.Core;assembly=Microsoft.Expression.Interactions"    
             xmlns:im="clr-namespace:Microsoft.Expression.Interactivity.Media;assembly=Microsoft.Expression.Interactions"
             x:Name="PanelScroller"             
             vm:ViewModelLocator.AutoWireViewModel="True">
    
    <UserControl.Resources>
        <BitmapImage x:Key="ButtonImage" UriSource="/Images/radio_button.png"/>
        <BitmapImage x:Key="ButtonImageHighlighted" UriSource="/Images/radio_button_highlighted.png"/>

        <Storyboard x:Key="OnScrollLeft">
            <DoubleAnimation
        		Storyboard.TargetName="ButtonHighlightedTranslateTransform"
        		Storyboard.TargetProperty="X"
        		By="{Binding ElementName=PanelScroller, Path=ScrollLeftWidth}"
        		Duration="0:0:0.001"/>
        </Storyboard>

        <Storyboard x:Key="OnScrollRight">
            <DoubleAnimation 
        		Storyboard.TargetName="ButtonHighlightedTranslateTransform"
        		Storyboard.TargetProperty="X"
        		By="{Binding ElementName=PanelScroller, Path=ScrollRightWidth}"
        		Duration="0:0:0.001"/>
        </Storyboard>

        <Storyboard x:Key="OnScrollLeftWrap">
            <DoubleAnimation 
        		Storyboard.TargetName="ButtonHighlightedTranslateTransform"
        		Storyboard.TargetProperty="X"
        		By="{Binding ElementName=PanelScroller, Path=ScrollToRightEndWidth}"
        		Duration="0:0:0.001"/>
        </Storyboard>

        <Storyboard x:Key="OnScrollRightWrap">
            <DoubleAnimation 
        		Storyboard.TargetName="ButtonHighlightedTranslateTransform"
        		Storyboard.TargetProperty="X"
        		By="{Binding ElementName=PanelScroller, Path=ScrollToLeftEndWidth}"
        		Duration="0:0:0.001"/>
        </Storyboard>
    </UserControl.Resources>
    
    <!-- Using Blend interactivity library triggers for invoking commands.  This enables me to start
    a Storyboard and invoke a command which I couldn't find a way to do with standard DataTriggers.
    It also supports invoking a command once a Storyboard is completed sticking to the MVVM principles
    rather than using the Completed event handler in the code behind for a Storyboard -->
    <i:Interaction.Triggers>
        <ic:DataTrigger Binding="{Binding NumPanels}" Comparison="NotEqual" Value="{Binding ElementName=PanelScroller, Path=NumButtons}">
            <ic:ChangePropertyAction PropertyName="NumButtons" Value="{Binding NumPanels}" />
            <ic:CallMethodAction MethodName="UpdateDimensions" />
        </ic:DataTrigger>
        
        <ic:DataTrigger Binding="{Binding ScrollLeft}" Value="True">
            <im:ControlStoryboardAction Storyboard="{StaticResource OnScrollLeft}" />
        </ic:DataTrigger>

        <ic:DataTrigger Binding="{Binding ScrollRight}" Value="True">
            <im:ControlStoryboardAction Storyboard="{StaticResource OnScrollRight}"/>
        </ic:DataTrigger>

        <ic:DataTrigger Binding="{Binding ScrollLeftWrap}" Value="True">
            <im:ControlStoryboardAction Storyboard="{StaticResource OnScrollLeftWrap}"/>
        </ic:DataTrigger>

        <ic:DataTrigger Binding="{Binding ScrollRightWrap}" Value="True">
            <im:ControlStoryboardAction Storyboard="{StaticResource OnScrollRightWrap}"/>
        </ic:DataTrigger>
    </i:Interaction.Triggers>

    <Grid Width="{Binding ElementName=PanelScroller, Path=TotalWidth}">
        <Grid.RowDefinitions>
            <RowDefinition Height="{Binding ElementName=PanelScroller, Path=ButtonWidthHeight}"/>
        </Grid.RowDefinitions>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="{Binding ElementName=PanelScroller, Path=ButtonWidthHeight}"/>
            <ColumnDefinition Width="*"/>
            <ColumnDefinition Width="{Binding ElementName=PanelScroller, Path=ButtonWidthHeight}"/>
        </Grid.ColumnDefinitions>
        <Grid Grid.Row="0" Grid.Column="0">
            <v:ArrowControl
                x:Name="LeftArrow"
                HorizontalAlignment="Left"
                VerticalAlignment="Center"
                Direction="Left"
                Width="{Binding ElementName=PanelScroller, Path=ButtonWidthHeight}"
                Height="{Binding ElementName=PanelScroller, Path=ButtonWidthHeight}">
                <i:Interaction.Triggers>
                    <i:EventTrigger EventName="PreviewMouseDown">
                        <i:InvokeCommandAction Command="{Binding ScrollLeftCommand}"/>
                    </i:EventTrigger>
                </i:Interaction.Triggers>
            </v:ArrowControl>
        </Grid>
        <Grid Grid.Row="0" Grid.Column="1">
            <Border>
                <Border.Background>
                    <ImageBrush
                        ImageSource="{StaticResource ButtonImage}"
                        TileMode="Tile"
                        AlignmentX="Center"
                        Stretch="Fill"
                        Viewport="{Binding ElementName=PanelScroller, Path=ViewportRect}"
                        ViewportUnits="Absolute"
                        Viewbox="{Binding ElementName=PanelScroller, Path=ViewboxRect}"
                        ViewboxUnits="RelativeToBoundingBox"/>
                </Border.Background>
            </Border>
            <Image x:Name="ButtonHightlighted"
                   Source="{StaticResource ButtonImageHighlighted}"
                   Width="{Binding ElementName=PanelScroller, Path=HighlightedButtonWidthHeight}"
                   Height="{Binding ElementName=PanelScroller, Path=HighlightedButtonWidthHeight}"
                   HorizontalAlignment="Left"
                   Stretch="Fill"
                   Margin="{Binding ElementName=PanelScroller, Path=ButtonMargin}" >
                <Image.RenderTransform>
                    <TransformGroup>
                        <TranslateTransform x:Name="ButtonHighlightedTranslateTransform"/>
                    </TransformGroup>
                </Image.RenderTransform>
            </Image>
        </Grid>
        <Grid Grid.Row="0" Grid.Column="2">
            <v:ArrowControl
                x:Name="RightArrow"
                HorizontalAlignment="Right"
                VerticalAlignment="Center"
                Direction="Right"
                Width="{Binding ElementName=PanelScroller, Path=ButtonWidthHeight}"
                Height="{Binding ElementName=PanelScroller, Path=ButtonWidthHeight}" RenderTransformOrigin="0.5,0.5">
                <v:ArrowControl.RenderTransform>
                    <ScaleTransform ScaleX="-1" ScaleY="1"/>
                </v:ArrowControl.RenderTransform>

                <i:Interaction.Triggers>
                    <i:EventTrigger EventName="PreviewMouseDown">
                        <i:InvokeCommandAction Command="{Binding ScrollRightCommand}"/>
                    </i:EventTrigger>
                </i:Interaction.Triggers>
            </v:ArrowControl>
        </Grid>
    </Grid>
</UserControl>