﻿using System;
using ScrollingAnimationsMVVMLight.Utiltilities;
using Xunit;

namespace ScrollingAnimationsMVVMLight.Tests
{
    public class PanelScrollerControl_Unit
    {
        [Fact]
        [Trait("Category", "Unit")]
        void Ctr_NullMessenger_ThrowsArgumentNullException()
        {
            Action createPanelScrollerControl = () => PanelScrollerControlBuilder.Create().Build();

            Assert.Throws<ArgumentNullException>("messenger", createPanelScrollerControl);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void NumPanels_ZeroPanelCount_ThrowsArgumentException()
        {
            var messenger = TestMessengerBuilder.Create().Build();
            var control = PanelScrollerControlBuilder.Create().WithMessenger(messenger).Build();
            Action setNumPanelsAction = () => control.NumPanels = 0;

            Assert.Throws<ArgumentException>("NumPanels", setNumPanelsAction);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollLeftCommand_Invoked_RaisesScrollLeftEvent()
        {
            var eventRaised = false;
            var messenger = TestMessengerBuilder.Create().WithScrollEventHandler((s) => { if (s.Content == Scroll.Direction.Left) eventRaised = true; }).Build();
            var control = PanelScrollerControlBuilder.Create().WithMessenger(messenger).Build();

            control.ScrollLeftCommand.Execute(this);

            Assert.True(eventRaised);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollLeftCommands_InvokedBeforeFirstScrollCompletes_IgnoresScrollCommandsWhileStillScrolling()
        {
            var eventRaisedCount = 0;
            var messenger = TestMessengerBuilder.Create().WithScrollEventHandler((s) => { if (s.Content == Scroll.Direction.Left) ++eventRaisedCount; }).Build();
            var control = PanelScrollerControlBuilder.Create().WithMessenger(messenger).Build();

            control.ScrollLeftCommand.Execute(this);
            control.ScrollLeftCommand.Execute(this);
            control.ScrollLeftCommand.Execute(this);

            Assert.Equal(eventRaisedCount, 1);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void MultipleScrollRightEvents_InvokedAfterEachPreviousScrollCompletes_RaisesScrollEventEachTime()
        {
            var eventRaisedCount = 0;
            var messenger = TestMessengerBuilder.Create().WithScrollEventHandler((s) => { if(s.Content == Scroll.Direction.Right) ++eventRaisedCount; }).Build();
            var control = PanelScrollerControlBuilder.Create().WithMessenger(messenger).Build();

            control.ScrollLeftCommand.Execute(this);
            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Right));
            control.ScrollLeftCommand.Execute(this);
            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Right));
            control.ScrollLeftCommand.Execute(this);
            messenger.Send<ScrollEvent>(new ScrollEvent(Scroll.Direction.Right));

            Assert.Equal(eventRaisedCount, 3);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void ScrollRightCommand_Invoked_RaisesScrollRightEvent()
        { 
            var eventRaised = false;
            var messenger = TestMessengerBuilder.Create().WithScrollEventHandler((s) => { if (s.Content == Scroll.Direction.Right) eventRaised = true; }).Build();
            var control = PanelScrollerControlBuilder.Create().WithMessenger(messenger).Build();

            control.ScrollRightCommand.Execute(this);

            Assert.True(eventRaised);
        }

        [Fact]
        [Trait("Category", "Unit")]
        void FinishScrollingHandler_InvokedAfterStartingAScroll_ClearsScrollFlags()
        {
            var messenger = TestMessengerBuilder.Create().Build();
            var control = PanelScrollerControlBuilder.Create().WithMessenger(messenger).WithAllScrollFlagsSet().Build();

            control.ScrollRightCommand.Execute(this);
            messenger.Send<FinishScrollingEvent>(new FinishScrollingEvent());

            Assert.False(control.ScrollLeft);
            Assert.False(control.ScrollLeftWrap);
            Assert.False(control.ScrollRight);
            Assert.False(control.ScrollRightWrap);
        }
    }
}
