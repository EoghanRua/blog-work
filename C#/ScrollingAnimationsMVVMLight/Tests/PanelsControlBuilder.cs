﻿using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;
using ScrollingAnimationsMVVMLight.Services;
using ScrollingAnimationsMVVMLight.Utiltilities;
using ScrollingAnimationsMVVMLight.ViewModels;

namespace ScrollingAnimationsMVVMLight.Tests
{
    public class PanelsControlBuilder
    {
        private IMessenger mMessenger;
        private SystemWrapperBase mSystemWrapper = new SystemWrapper();
        private IPanelLoaderService mPanelLoaderService = null;

        public static PanelsControlBuilder Create()
        {
            return new PanelsControlBuilder();
        }

        public PanelsControlBuilder WithMessenger(IMessenger messenger)
        {
            mMessenger = messenger;
            return this;
        }

        public PanelsControlBuilder WithPanels(List<PanelsPanel> panels)
        {
            mPanelLoaderService = new TestPanelLoaderService(panels);
            return this;
        }

        public PanelsControlBuilder WithPanelLoaderService(IPanelLoaderService service)
        {
            mPanelLoaderService = service;
            return this;
        }

        public PanelsControlBuilder WithSystemWrapper(SystemWrapperBase systemWrapper)
        {
            mSystemWrapper = systemWrapper;
            return this;
        }

        public PanelsControl Build()
        {
            return this;
        }

        public static implicit operator PanelsControl(PanelsControlBuilder builder)
        {
            var control = new PanelsControl(
                builder.mSystemWrapper,
                builder.mMessenger,
                builder.mPanelLoaderService);

            control.HookupImagesToPanels();

            return control;
        }
    }
}
