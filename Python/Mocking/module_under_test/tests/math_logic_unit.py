import unittest
from mock import patch, MagicMock

class IsEven(unittest.TestCase):
    """ Class testing is_even logic """

    @patch('dependency_module.dependency_with_function_to_mock.fn_to_mock'
                , new=MagicMock(side_effect=lambda: print("Skipping file IO\n")))
    def setUp(self):
        import module_under_test.math_logic as ml
        self.math_logic = ml

    def test_OddInput_ReturnsFalse(self):
        result = self.math_logic.is_even(3)

        self.assertFalse(result)

if __name__ == '__main__':
    unittest.main()
