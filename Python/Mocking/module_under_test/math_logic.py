""" Math utility functions """
import dependency_module.dependency_with_function_to_mock as dep

dep.fn_to_mock()


def is_even(value):
    """ check if a value is even or not """
    dep.log_data(value)
    return (value % 2) == 0
