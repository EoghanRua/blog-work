import unittest

# load the maya libraries that allow a python script to run a windowless
# version of maya this will allow our unit tests to run from a command line
import maya.standalone as ms

# attempt to intiliaze the standalone libraries, if this script is
# being run insde of maya then maya will throw an exception
try:
    ms.initialize(name='python')
except:
    pass

# load the pymel core for the default maya commands that
# will be used for setting up the various tests
import pymel.core as pm

import SimplePyMEL

class SimplePyMELTest(unittest.TestCase):
    
    def tearDown(self):
        # Cleanup the scene
        testNodes = pm.ls(dag=True, v=True)
        if testNodes:
            pm.delete(testNodes)

    def test_CreateParentConstraint_SetsDriverJointAsTarget_ForDriverAndDrivenJoints(self):
        # Arrange:
        driverPos = [0.0, 5.0, 0.0]
        drivenPos = [2.0, 7.0, 2.0]

        # Act:
        driver, driven = SimplePyMEL.createParentConstraintSetup(driverPos, drivenPos)

        # Assert:
        parentConstraints = pm.ls(typ="parentConstraint")
        targets = pm.parentConstraint(parentConstraints[0], q=True, tl=True)        
        self.assertEquals(driver, targets[0], "The new driver (%s) doesn't match the target (%s) of the parent constraint. Has the Parent Constraint been setup correctly?" % (driver, targets[0]))

    def test_CreateParentConstraint_ConstrainsChildPositionToParentPosition_ForJoints(self):
        # Arrange:
        driverPos = [0.0, 5.0, 0.0]
        drivenPos = [2.0, 7.0, 2.0]
        driver, driven = SimplePyMEL.createParentConstraintSetup(driverPos, drivenPos)

        # Act:
        offset = [1.0, -3.0, 6.1]
        newDriverPos = driverPos
        for i in range(len(newDriverPos)):
           newDriverPos[i] += offset[i]
        pm.joint(driver, e=True, p=newDriverPos)

        # Assert:
        result = pm.joint(driven, q=True, p=True)
        expected = drivenPos
        for i in range(len(expected)):
            expected[i] += offset[i];
        self.assertEquals(expected, result, "The new driven joint position (%s) doesn't match the expected position (%s). Has the Parent Constraint been setup correctly?" % (result, expected))

if __name__ == '__main__':
    unittest.main(verbosity=2)
