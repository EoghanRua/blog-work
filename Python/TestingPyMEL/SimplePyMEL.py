import pymel.core as pm

def createParentConstraintSetup(driver_position, driven_position):

    driver = pm.joint(p=driver_position, n="driver#")

    # Clear selection before creating a new joint
    pm.select(cl=True)
    driven = pm.joint(p=driven_position, n="driven#")

    pm.parentConstraint(driver, driven, mo=True, n="#parent_constraint")

    return driver, driven


