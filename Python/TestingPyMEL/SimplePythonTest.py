import unittest
import sys
import SimplePython

class SimplePythonTest(unittest.TestCase):
    def test_AddValues_ReturnsSum_ForTwoIntegers(self):
        # Arrange:
        a = 10
        b = 20
        expected = a + b

        # Act:
        result = SimplePython.addValues(a, b)

        # Assert:
        self.assertEquals(expected, result, "The sum of the two values should be equal to the result of the add_values function")
        
    def test_AddValues_RaisesOverFlowError_ForLargeFloats(self):
        # Arrange:
        a = sys.maxint ** 10.0
        b = sys.maxint ** 10.0

        # Act/Assert:
        self.assertRaises(OverflowError, SimplePython.addValues, a, b)

if __name__ == '__main__':
    unittest.main(verbosity=2)